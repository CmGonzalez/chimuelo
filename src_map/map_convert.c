/* Tool to import from Tiled JSON to a Compressed and compatible C struc
 * For Chimuelo Engine
 * Created by Noentiendo
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "parson/parson.h"
#include "zx7/zx7.h"


int main(int argc, char *argv[])
{
	JSON_Array	*layers;
	JSON_Array	*data;
	JSON_Array	*objects;

	JSON_Object *map;
	/*JSON_Object *screen;*/
	JSON_Object *layer;
	JSON_Object *object;

  JSON_Value	*root_value;

	const char *name = NULL;
	int tile_height;
	int tile_width;

	int screen_height;
	int screen_width;
	int screen_total;

	int screen_xi;
	int screen_yi;
	int screen_xe;
	int screen_ye;
	int screen_x;
	int screen_y;


	int map_height;
	int map_width;
	int map_screens_h;
	int map_screens_v;
	char curr_scr;

	size_t i;
	size_t j;
	int k;
	int s;

	int zxi;

	char *rawdata;
	unsigned char *zxdata;
	unsigned char *zxdata_pack;
	unsigned char *zxdata_7zx;
	unsigned int *zxdata_index;

	size_t input_size;
	size_t output_size;
	size_t total_size;

	int rawsize;
	/* File Handling */
	FILE *ifp;
	FILE *ofp;
	char *output_name;


  /* determine output filename */
	if (argc == 2) {
		output_name = "game_map.h";
	} else if (argc == 3) {
		output_name = argv[2];
	} else {
		 fprintf(stderr, "Usage: %s input.json [game_map.h]\n", argv[0]);
		 exit(1);
	}

	/* open input file */
	ifp = fopen(argv[1], "rb");
	if (!ifp) {
		 fprintf(stderr, "Error: Cannot access input file %s\n", argv[1]);
		 exit(1);
	}

	/* determine input size */
	fseek(ifp, 0L, SEEK_END);
	input_size = ftell(ifp);
	fseek(ifp, 0L, SEEK_SET);
	if (!input_size) {
		 fprintf(stderr, "Error: Empty input file %s\n", argv[1]);
		 exit(1);
	}

	/* close input file */
	fclose(ifp);
	 /* parsing json and validating output */
	root_value = json_parse_file(argv[1]);
	/* parsing map to json object */
	map = json_value_get_object(root_value);
	tile_width = json_object_get_number(map,"tilewidth");
	tile_height = json_object_get_number(map,"tileheight");

	/* get layers array */
	layers = json_object_get_array(map,"layers");
	if (layers != NULL) {
		printf("Processing Map.\n");
		/* loop at layers */
		for (i = 0; i < json_array_get_count(layers); i++) {
			/* retrieve layer */
			layer = json_array_get_object(layers,i);
			name = json_object_get_string(layer,"name");
			/* Processing Background Layer - Tile map */
			if (strcmp( name,  "Background") == 0) {
				/* allocate raw map memory */

				map_width = json_object_get_number(layer,"width");
				map_height = json_object_get_number(layer,"height");
				rawdata = (char*) malloc(map_width * map_height * sizeof(char));
				/* Processing Background */
				data = json_object_get_array(layer, "data");
				rawsize = json_array_get_count(data);
				for (j = 0; j < rawsize; j++) {
					/* append to a c array for proccesing */
					*(rawdata +j) = json_array_get_number(data,j);
				}
			}
      /* Processing Helper Layer - Screen distribution on map */
			if (strcmp( name,  "Helper") == 0 && rawsize) {
				objects = json_object_get_array(layer, "objects");
				zxi = 0;
				/* get data from first screen on helper (0,0), not sure if allways is ok? */
				if (json_array_get_count(objects) > 0) {
					screen_total = json_array_get_count(objects);
					objects = json_object_get_array(layer, "objects");
					object = json_array_get_object( objects, 0);
					screen_width = json_object_get_number(object,"width");
					screen_height = json_object_get_number(object,"height");
					screen_width = screen_width / tile_width;
					screen_height = screen_height / tile_height;
					map_screens_h = map_width / screen_width;
					map_screens_v = map_height / screen_height;
					zxdata = (unsigned char*) malloc(map_width * map_height * sizeof(char));
					zxdata_pack = (unsigned char*) malloc(map_width * map_height * sizeof(char));
					zxdata_index = (unsigned int*) malloc(screen_total * sizeof(unsigned int));
				}

				for (j = 0; j < json_array_get_count(objects); j++) {
					object = json_array_get_object( objects, j);
					screen_xi = json_object_get_number(object,"x") / tile_width;
					screen_yi = json_object_get_number(object,"y") / tile_width;
					screen_xe = screen_xi + screen_width;
					screen_ye = screen_yi + screen_height;
					/* Get Data Array */
					data = json_object_get_array(object,"data");
					curr_scr = (((screen_yi* map_screens_v) / map_height) * map_screens_h) + (screen_xi / screen_width);
					zxi = curr_scr * screen_width * screen_height;
					/* Proccesing Screen - pack to zx array*/
					for (screen_y = screen_yi; screen_y < screen_ye; screen_y++) {
						/* loop rows */
						for (screen_x = screen_xi; screen_x < screen_xe; screen_x++) {
							/* loop columns */
							k = ( screen_y * map_width ) + screen_x;
							*(zxdata + zxi) = *(rawdata + k);
							zxi++;
						}
					}
				}
			}
			/* Processing Background Layer - Tile map */
			if (strcmp( name,  "Entities") == 0) {
				objects  = json_object_get_array(layer, "objects");
					printf("%i\n", json_array_get_count(objects));
				for (j = 0; j < json_array_get_count(objects); j++) {
					object = json_array_get_object( objects, j );
					printf("%s\n", json_object_get_string(object,"type"));
				}
printf("%i\n", json_array_get_count(objects));
				//entities = json_object_get_array(layer, "objects");
        //printf("%s\n", json_object_get_string(screen,"type"));
			}

		}

		/* Pack array */
		/* generate output file */

		/*
		printf("screen_width %i\n", screen_width);
		printf("screen_height %i\n", screen_height);
		printf("map_screens_h %i\n", map_screens_h);
		printf("map_screens_v %i\n", map_screens_v);
		printf("input_size %i\n", input_size);
		*/

		k = 0;
		*(zxdata_index) = 0;
		zxi = 0;
		input_size = screen_width * screen_height  * sizeof(char);

		//ofp = fopen("out.bin", "wb");
		//fwrite (zxdata, 1, (screen_width * screen_height), ofp);
		//fclose(ofp);

		while( k < (screen_width * screen_height * map_screens_h * map_screens_v) ) {
			output_size = 0;
			zxdata_7zx = compress(optimize( (zxdata + k), input_size ), (zxdata + k), input_size, &output_size);
			memcpy(zxdata_pack+total_size,zxdata_7zx,output_size);
			k = k + input_size;
			total_size = (unsigned int) total_size + (unsigned int) output_size;
			zxi = zxi + sizeof(size_t);
			*(zxdata_index + zxi) = total_size;
		}

		ofp = fopen(output_name, "w");
		fprintf(ofp,"/* Map Data\n");
		fprintf(ofp," * Tot Screens   : %i\n",screen_total);
		fprintf(ofp," * Unpack Size   : %i\n", (unsigned int) input_size * map_screens_h * map_screens_v);
		fprintf(ofp," * Packed Size   : %i\n", (unsigned int) total_size);
		fprintf(ofp," * Map Columns   : %i\n", map_screens_h);
		fprintf(ofp," * Map Rows      : %i\n", map_screens_v);
		fprintf(ofp," * Screen Width  : %i\n", screen_width);
		fprintf(ofp," * Screen Height : %i\n", screen_height);
		fprintf(ofp,"*/\n");
		fprintf(ofp,"\n");

		fprintf(ofp,"const unsigned int lenght0[] = {\n");
		s=0;
		zxi=0;
		while( s < screen_total ) {
			fprintf(ofp,"%d", *(zxdata_index + zxi ));
			zxi = zxi + sizeof(size_t);
			if (s < (screen_total-1)) fprintf(ofp,",");
			s++;
			if (s % map_screens_h == 0){
				fprintf(ofp,"\n");
			}
		}
		fprintf(ofp,"};\n");

		fprintf(ofp,"const unsigned char world0[] = {\n");
		s=1;
		zxi = 0;
		total_size = 0;
		while( s < screen_total ) {
			j = 0;
			j = total_size; /*Old Value*/
			total_size =  *(zxdata_index + ( sizeof(size_t) * s ) );
			while( j < total_size ) {
				fprintf( ofp,"%d", *(zxdata_pack + j ) );
				j++;
				if ( j < (total_size) )fprintf(ofp,",");
			}
			if (s < (screen_total - 1)) fprintf(ofp,",");
			fprintf(ofp,"\n");
			s++;
		}
		fprintf(ofp,"};\n");
		fclose(ofp);
		printf("Tot Screens: %i\n", screen_total);
		printf("Unpack Size: %i\n", (int) input_size * map_screens_h * map_screens_v);
		printf("Packed Size: %i\n", (int) total_size);
	} else {
		printf("Error retrieving Layers.\n");
	}
	/* cleanup code */

	/*free(rawdata);*/
	/*free(zxdata);*/



	return 0;
}
