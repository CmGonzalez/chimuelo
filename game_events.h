/*
   This file is part of PGD.

   PGD is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   PGD is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with PGD.  If not, see <http://www.gnu.org/licenses/>.
 */



#ifndef GAME_EVENTS_H
#define GAME_EVENTS_H

#include "globals.h"
#include "src_engine/player.h"

extern unsigned char player_move(void);
extern void player_pick_spr();
extern unsigned char game_init();
extern void game_event();

#endif
