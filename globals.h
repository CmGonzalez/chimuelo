/*
   This file is part of Chimuelo Engine v3.0.

   Chimuelo Engine is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Chimuelo Engine is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with PGD.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GLOBALS_H
#define GLOBALS_H
#include <input.h>

/* ENGINE DEFINES - Enable/Disable Engine Features */
/* Platform defines - Enable to activate codeblocks*/

/*Enable Jump code, ie player jumps*/
#define GAME_JUMP                    1
/*Enable Gravity code, ie player falls*/
#define GAME_GRAVITY                 1
/* Enable to enter debug mode - comment to disable debug */
#define GAME_DEBUG
/* Enable allow continue coded - Manic Pietro based */
#define GAME_CODES
/*Loop horizontal movement*/
#define SCR_LOOP_X                   1
/*Loop vertical movement*/
#define SCR_LOOP_Y                   1

/* Audio Modes */
/* Enable To Activate Beeper Effects + BeepFX (EXPERIMENTAL) */
//#define AUDIO_BEEPER

/* Enable To Activate mFX Player MUSIC(2ch) + FX(1ch) */
#define AUDIO_MFX

/* Samples encoded from wavs + midi import */
//#define AUDIO_VOICENC

/* Maps stored on bank0 - 48 Compatible */
//#define MAP_BANK0

/* Maps stored on bank6 - 128 Only */
#define MAP_BANK6

/* Used to add or substract to the tiles from json mapfile */
#define MAP_OFFSET_TILE             33

/* Screen Geometry */
/* Game Columns Max 32 */
#define SCR_COLS                     20//Max 32   // COLUMNS
/* Game Rows Max (24 + 2) fake rows for Nirvana Engine beacuse it 0lin is on -8px screen */
#define SCR_ROWS                     24
/* Game Lin Floor, sprites won't be allowed below it */
#define SCR_LINS                     184
/* Shift the screen n columns to the right */
#define SCR_OFFSET_X                 6




#define MAX_OBJECTS                  1 //OBJECTS OR COINS

/* Player indexes */
#define INDEX_P1                     5 //Index of P1 == GAME_MAX_PRITES - 1
#define INDEX_FRUIT                  4 //Index of P1 == GAME_MAX_PRITES - 1

//Nirvana Related
#define NIRV_TOTAL_SPRITES           7 //Maximun of sprites to handle (Nirvana == 8) (Transparency Nirvana == 7)
#define NIRV_SPRITE_FG               6 //Sprite to be used by Foreground
#define NIRV_SPRITE_P1               5 //Sprite with Paper Transparency



#define GAME_MAX_SPRITES             6 //MAX OF OBJECTS (ENEMIES + PLAYERS)
#define GAME_MAX_ENEMIES             5 //GAME_MAX_SPRITES - PLAYERS
#define GAME_MAX_TILES              32 //Number of tiles 8x8 gfx
#define GAME_VELOCITY               -6
#define GAME_START_LIVES             3
#define GAME_COLLISION_TIME         30 //TIME BTWN COLISION CHECKS
#define GAME_ENEMY_MAX_CLASSES        30
#define GAME_ENEMY_CLASS_LEN          9 // lenght of attributes of each class
#define GAME_TOTAL_CLASSES            6 //Total class of enemies
#define GAME_TOTAL_INDEX_CLASSES      30 //Total class of enemies variations, the same enemy can be left/right etc...
#define GAME_SPR_INIT_SIZE            9
#define GAME_MAX_VALUES               18 //Max Values for stage Data
// Game Event timer called n ms aprox
#define GAME_EVENT                    100

#define PLAYER_SPEED                 3
#define ENEMY_FAST_SPEED             0



//TILES CLASSES
#define TILE_EMPTY                   0  //BACKGROUND
#define TILE_FLOOR                   1  //PLATFORMS
#define TILE_CRUMB                   2  //CRUMBLING PLATFORMS
#define TILE_WALL                    3  //SOLID WALL
#define TILE_CONVEYOR                4  //CONVEYORS
#define TILE_DEADLY                  5  //DEADLY
#define TILE_DOT                     6  //EXTRA SWITCH
#define TILE_OBJECT                  8  //KEY
#define TILE_PILL                    9  //PILL
#define TILE_CRUMB_INIT              2 //TODO MOVE ON BTILE AND PLACE ADJACENT TO CRUMBLING TILES



#define E_NONE                        0
#define E_HORIZONTAL                  1
#define E_VERTICAL                    2
#define E_GHOST                       3
#define E_EYES                        4
#define E_WIMP                        5
#define E_WIMP_ALT                    6
#define E_PICKEABLE                   7
#define E_PLAYER                      255

#define TILE_TITLE                    204
#define TILE_ANIM                     128

/* Player 1 tiles */
#define TILE_P1_RIGHT                 16 //TILE OF P1


/* General */

//Starting Directions
#define DIR_NONE                      0
#define DIR_LEFT                      1
#define DIR_RIGHT                     2
#define DIR_UP                        3
#define DIR_DOWN                      4
#define DIR_FIRE                      5

/*Stats (bits)*/
#define BIT_UP                        1
#define BIT_DOWN                      2
#define BIT_LEFT                      4
#define BIT_RIGHT                     8
#define BIT_JUMP                      16
#define BIT_ATTACK                     32
#define BIT_CLEAR                     64
#define BIT_DEAD                      128

//Two bit secuences
#define BIT_VERT                      3
#define BIT_HORIZ                     12

/*Stats alt (bits)*/
#define BIT1                          1
#define BIT2                          2
#define BIT3                          4
#define BIT4                          8
#define BIT5                          16
#define BIT6                          32
#define BIT7                          64
#define BIT8                          128

/* Player */


/* Enemies */
#define ENEMIES_MAX                   5   //MAX QUANTITY OF ENEMIES ON SCREEN (0->6)
#define ENEMIES_MAXJUMP               12 //MAX JUMP WHEN HIT
#define ENEMY_JUMP_SPEED              1
#define ENEMY_FALL_SPEED              1
#define ENEMY_KILLED_SPEED            8

// Sound Mode Variables
#define GAME_SOUND_48_FX_ON          0x01
#define GAME_SOUND_48_FX_OFF         0xfe
#define GAME_SOUND_48_MUS_ON         0x02
#define GAME_SOUND_48_MUS_OFF        0xfd
#define GAME_SOUND_AY_FX_ON          0x04
#define GAME_SOUND_AY_FX_OFF         0xfb
#define GAME_SOUND_AY_MUS_ON         0x08
#define GAME_SOUND_AY_MUS_OFF        0xf7


#ifdef GAME_JUMP
/*Number of horizontal (2px) of the Jump*/
#define GAME_JUMP_HOR                12
#endif

struct Sprite {
  unsigned char class; // CLASS OF SPRITE
   /* SPRITE STATES SEE DEFINES UPPER BIT VALUES */
  unsigned char state;
  unsigned char state_l;


   // MISC SPRITE VALUES  MIN COL/LIN
  unsigned char value_a;
  unsigned char value_b;
  unsigned char value_c;
  unsigned char value_d;
  unsigned char value_e;

  unsigned char base_tile;

  unsigned char spr_speed;

  unsigned int last_time;   // LAST TIME OF MOVEMENT FOR ANIMATIONS / SPEED
  unsigned int last_time_a; // User Timer A
  unsigned int last_time_b; // User Timer B

  unsigned char spr_frames;
  unsigned char spr_lin_inc;
  unsigned char spr_altset;
  unsigned char spr_kind;

  unsigned char tile; // TILE
  unsigned char lin;  // LINE

  unsigned char col;      // COLUMN
  unsigned char colint;   // INTERNAL COLUMN/TILE INCREMENT
  unsigned unsigned char vert_anim;       //VERTICAL ANIMATIONS on/off
};
extern struct Sprite sprite1;
extern struct Sprite sprite2;
extern struct Sprite sprite3;
extern struct Sprite sprite4;
extern struct Sprite sprite5;
extern struct Sprite sprite6;

extern struct Sprite *sprites[];
extern struct Sprite *sprite;


extern unsigned char spec128;
//General Use VARIABLES
extern unsigned char i;
extern unsigned char v0;
extern unsigned char v1;
extern unsigned char v2;

extern unsigned char btiles[];
extern uint16_t (*joyfunc1)(udk_t *); //TODO REMOVE THIS AS IS PART NOW OF input.h
extern uint16_t (*joyfunc2)(udk_t *); //TODO REMOVE THIS AS IS PART NOW OF input.h
extern udk_t k1;
extern udk_t k2;


extern unsigned char dirs;




extern unsigned char obj_count;

extern unsigned char tbuffer[7];

extern uint64_t player_score;




//PLAYER ATTRIBUTES




extern unsigned char player_start_col;
extern unsigned char player_start_lin;
extern unsigned char player_start_tile;


extern unsigned char game_song_play;
extern unsigned char game_exit_col;
extern unsigned char game_exit_lin;
extern unsigned char game_code;

extern unsigned char game_round_up;
extern unsigned char menu_curr_sel;
extern unsigned char game_atrac;
extern unsigned char game_menu;
extern unsigned char game_audio_mode;
extern unsigned char game_mincol;
extern unsigned char game_maxcol;





extern unsigned char spr_index;
extern unsigned char game_debug;




extern unsigned char s_lin0;
extern unsigned char s_col0;
extern unsigned char s_colint0;

extern unsigned char s_col1;
extern unsigned char s_lin1;
extern unsigned char menu_row;


extern unsigned int loop_count;
extern unsigned int index0;
extern unsigned int index1;

extern unsigned char zx_val_asm;

//Attrib Arrays
extern unsigned char attrib[4];
extern unsigned char menu_attrib[4];



//TODO REVIEW 128 bytes! needs loop on final target without udg's

extern unsigned int curr_time;
extern unsigned int time_event;

extern unsigned char spr_count;


extern unsigned char game_inmune;
extern unsigned char game_inf_lives;
extern unsigned char game_sound;
extern unsigned char game_tile_cnt;

extern unsigned char game_over;

extern unsigned char player_lives;
extern unsigned char player_coins;
extern uint64_t game_score_top;
extern uint64_t player_next_extra;
extern unsigned char game_start_scr;


extern unsigned char screen_paper, screen_ink;
extern const unsigned char spr_init[];


extern unsigned char scr_map[];


//extern unsigned char scr_obj0[];
//extern unsigned char scr_obj1[];



//Bank 6 Level Data
extern const unsigned char world0[];
extern const unsigned int lenght0[];

extern unsigned char scr_curr;
extern unsigned char *gbyte;




extern unsigned char game_border;
extern unsigned char game_ink;
extern unsigned char game_paper;
extern unsigned char map_clear;


extern unsigned char tile_class[];


extern unsigned char *attribs;
extern unsigned char *deltas;


extern unsigned char game_tune;
extern unsigned char game_beep;

extern unsigned char *spr8_lookup_s[];
extern unsigned char *spr8_lookup_e[];

#ifdef GAME_CODES
extern const unsigned char game_encode[];
#endif

#ifdef GAME_JUMP
extern signed char player_jump_vert[];
extern unsigned char player_jump_count;
extern unsigned char player_jump_lin;
extern unsigned char player_eat_count;
extern unsigned char player_jump_top;
#endif

#ifdef GAME_DEBUG
extern unsigned char debug_show;
extern unsigned int fps;
#endif
// End of GLOBALS_H
#endif
