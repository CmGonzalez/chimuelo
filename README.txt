==============================================================================

README - CHGD v0.1


Chimuelo Game Designer is a ZX Spectrum game dev framework based on C
Using the z88dk compiler v1.99C
Using regular or custom versions of NIRVANA+ for the multicolour graphic engine.

==============================================================================

BUILD INSTRUCTIONS:

* Install or update to the current Z88DK
https://www.z88dk.org/forum/viewtopic.php?id=10706
https://github.com/z88dk/z88dk/releases/tag/v1.99c
https://sourceforge.net/projects/z88dk/files/z88dk/1.99C/
https://github.com/z88dk/z88dk#installation

Compile z88dk
unpack z88dk
cd z88dk
chmod 777 build.sh
chmod 777 config.sh
./build.sh

Git clone if you will regularly update z88dk.
Nightly build if you just want to unzip the current build.

PGD uses zsdcc so linux users should make sure they also build zsdcc.
zsdcc is included among the pre-built Windows and OSX binaries already.

* Configure the nirvana+ library

Edit file "z88dk/libsrc/_DEVELOPMENT/target/zx/config_nirvanap.m4"
Change "define(`__NIRVANAP_TOTAL_ROWS', 23)" to "define(`__NIRVANAP_TOTAL_ROWS', 18)”

You can also disable wide tiles from Nirvana by changing, to save 300 bytes aprox.

define(`__NIRVANAP_OPTIONS_WIDE_DRAW',    0x00)
define(`__NIRVANAP_OPTIONS_WIDE_SPRITES', 0x00)

To build the Pentagon version instead of the regular Spectrum version:

Edit file "z88dk/libsrc/_DEVELOPMENT/target/zx/config_target.m4"
Change "define(`__SPECTRUM', 1)" to "define(`__SPECTRUM', 32)"

* Rebuild the zx library so that nirvana+ changes take effect

Open a shell and go to directory "z88dk/libsrc/_DEVELOPMENT"
Run "Winmake zx" (windows) or "make TARGET=zx" (anything else)

* Build

Open a shell in the game home directory
Run "make" to build taps.
Run "make zx7" (twice) to build zx7 compressed taps.
Run "make sna" to build 128k snapshot.

Windows users can build by running "zcompile" instead.

==============================================================================

RUNTIME MEMORY MAP:

23296 - 23583     Stack (288 bytes)
23584 - 56113ish  Game
56323 - 65378     Nirvana+ Engine (nirvana hole contains program variables)



128K ONLY:

BANK 3
49152 - ?

BANK 4
49152 - 65488     AY Music, Sound Effects and Players

BANK 6
49152 - 61895     AY Music, Sound Effects and Players, Map Data

==============================================================================



What's NEW

Version 0.01
Initial Commit
