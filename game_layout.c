/*
        This file is part of PGD.

        PGD is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        PGD is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with PGD.  If not, see <http://www.gnu.org/licenses/>.


        PGD - Cristian Gonzalez - cmgonzalez@gmail.com
 */

 #include "game.h"
 #include "src_engine/audio.h"
 #include "src_engine/banks.h"
 #include "src_engine/enemies.h"
 #include "src_engine/engine.h"
 #include "src_engine/menu.h"
 #include "src_engine/player.h"
 #include "src_engine/sprite.h"
 #include "src_engine/zx.h"
 #include "src_engine/macros.h"
 #include "game_events.h"
 #include "game_layout.h"

 #include <arch/zx.h>
 #include <arch/zx/nirvana+.h>
 #include <input.h>
 #include <stdlib.h>
 #include <compress/zx7.h>

void game_header(){
  /* Print Header */
  zx_print_ink(INK_RED | (game_border << 3) | BRIGHT);
  zx_print_str(0, 2, "SCORE");
  zx_print_str(0, 18, "HIGH");
  zx_print_ink(INK_WHITE | (game_border << 3) | BRIGHT);
  zx_print_str(0, 13, "0");
  zx_print_str(0, 29, "0");

  game_print_score();
}

void game_footer( ) {
  /* Print Footer */
  zx_print_ink(INK_BLUE | (game_border << 3) | BRIGHT);
}
