/*
        This file is part of PGD.

        PGD is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        PGD is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with PGD.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <input.h>
#include "game.h"
#include "globals.h"
#ifdef MAP_BANK0
#include "game_map.h"
#endif



/* General Use VARIABLES */
unsigned char i;
unsigned char v0;
unsigned char v1;
unsigned char v2;

unsigned char spr_index; //Sprite Index
 /* Used for ASM PRINT */
unsigned char tbuffer[7];
unsigned char spec128;

/* SCREEN GAME MAP */

unsigned char scr_map[SCR_COLS * SCR_ROWS];
unsigned char scr_curr;

/* CONTROL VARIABLES */
uint16_t (*joyfunc1)(udk_t *); // pointer to joystick function Player 1
uint16_t (*joyfunc2)(udk_t *); // pointer to joystick function for game_control_mode
udk_t k1;
udk_t k2;

unsigned char dirs;



struct Sprite sprite1;
struct Sprite sprite2;
struct Sprite sprite3;
struct Sprite sprite4;
struct Sprite sprite5;
struct Sprite sprite6;
struct Sprite *sprites[GAME_MAX_SPRITES] = {&sprite1, &sprite2, &sprite3, &sprite4, &sprite5, &sprite6};
struct Sprite *sprite;

unsigned char obj_count;

// PLAYER ONLY
uint64_t player_score; // SCORE
unsigned char player_lives;
unsigned char player_coins;

unsigned char player_start_col;
unsigned char player_start_lin;
unsigned char player_start_tile;


signed int gravity;
unsigned char game_song_play;
unsigned char game_exit_col;
unsigned char game_exit_lin;
unsigned char game_code;
unsigned char game_menu;
unsigned char game_tune;
unsigned char game_beep;
unsigned char game_audio_mode;
unsigned char game_mincol;
unsigned char game_maxcol;


unsigned char game_atrac;
unsigned char game_tunes;

unsigned char game_round_up;
unsigned char menu_curr_sel;


/* Sprite start values */
unsigned char s_lin0;
unsigned char s_col0;
unsigned char s_colint0;

unsigned char s_lin1;
unsigned char s_col1;
unsigned char menu_row;

unsigned int loop_count;

unsigned int index0;
unsigned int index1;

unsigned char zx_val_asm;
unsigned char attrib[4];
unsigned char menu_attrib[4];

unsigned char *gbyte;

unsigned int curr_time;
unsigned int time_event;

unsigned char spr_count;

//###############################################################################################
//# # # GAME VARIABLES
//# #
//#
//###############################################################################################

unsigned char game_inmune;
unsigned char game_inf_lives;

unsigned char game_sound;
unsigned char game_tile_cnt;
unsigned char game_over;
uint64_t game_score_top;



uint64_t player_next_extra;
unsigned char game_start_scr;

// PHASE RELATED VARIABLES
unsigned char screen_paper;
unsigned char screen_ink;

unsigned char game_border;
unsigned char game_ink;
unsigned char game_paper;
unsigned char map_clear;


//28*9
const unsigned char spr_init[GAME_ENEMY_MAX_CLASSES*GAME_ENEMY_CLASS_LEN];


unsigned char *attribs;
unsigned char *deltas;

#ifdef GAME_CODES
const unsigned char game_encode[10] = {
  'Z', //0
  'X', //1
  'S', //2
  'P', //3
  'E', //4
  'C', //5
  'T', //6
  'R', //7
  'U', //8
  'M'  //9
};
#endif


const unsigned char spr_init[] = {
    64,  24, 4,   4, E_HORIZONTAL, 0xFF, 0xFF, 0xFF, 0xFF, // GHOST CYAN
    66,  32, 4,   4, E_HORIZONTAL, 0x05, 0x07, 0xFF, 0xFF, // GHOST2
    68,  40, 4,   4, E_HORIZONTAL, 0x03, 0x02, 0x04, 0xFF, // GHOST3
    70,  48, 4,   4, E_HORIZONTAL, 0x03, 0x05, 0xFF, 0xFF, // GHOST4
};

//Nirvana spr8 Lookup tables
unsigned char *spr8_lookup_s[GAME_MAX_TILES];
unsigned char *spr8_lookup_e[GAME_MAX_TILES];
unsigned char tile_class[GAME_MAX_TILES];

#ifdef GAME_GRAVITY
signed   int  game_gravity;
#endif

#ifdef GAME_JUMP
//Jump Geometry HORIZONTAL extra increment to emulate Willy Jump , for Nirvana Allways use odd values
signed char player_jump_vert[GAME_JUMP_HOR] = { -8,-8, -6,-2, -2, 0,  0, 2,  2, 6,  8, 8 };
unsigned char player_jump_count;
unsigned char player_jump_lin;
unsigned char player_jump_top;
unsigned char player_eat_count;
#endif

#ifdef GAME_DEBUG
unsigned char debug_show;
unsigned int fps;
#endif
