; ----------------------------------------------------------------
; Z88DK INTERFACE LIBRARY FOR NIRVANA+ ENGINE - by Einar Saukas
;
; See "nirvana+.h" for further details
; ----------------------------------------------------------------

; void NIRVANAP_printQ(void *qtile, void *attrs, unsigned char lin, unsigned char col)

SECTION code_clib
SECTION code_nirvanap

PUBLIC _NIRVANAP_printQ

EXTERN asm_NIRVANAP_printQ

_NIRVANAP_printQ:

   pop af          ; af = ret
   pop hl          ; hl = qtile
   pop bc          ; bc = attrs
   pop de          ; d = col, e = lin
   push de
   push bc
   push hl
   push af
   ld a,d
   ld d,e          ; d = lin
   ld e,a          ; e = col

   jp asm_NIRVANAP_printQ
