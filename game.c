/*
        This file is part of PGD.

        PGD is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        PGD is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with PGD.  If not, see <http://www.gnu.org/licenses/>.


        PGD - Cristian Gonzalez - cmgonzalez@gmail.com
 */

#include "game.h"
#include "globals.h"
#include "game_events.h"
#include "game_layout.h"
#include "src_engine/audio.h"
#include "src_engine/banks.h"
#include "src_engine/enemies.h"
#include "src_engine/engine.h"
#include "src_engine/menu.h"
#include "src_engine/player.h"
#include "src_engine/sprite.h"
#include "src_engine/zx.h"
#include "src_engine/macros.h"


#include <arch/zx.h>
#include <arch/zx/nirvana+.h>
#include <input.h>
#include <stdlib.h>
#include <compress/zx7.h>
/*
  BIG TODO
  loading screen customization, convert to scr alternate format from SCR.
  Use of Strucst for Sprites n Sprite init variables

*/



void main(void) {
  //Tiles defintions, all are started with 0 or TILE_EMPTY
  tile_class[16] = TILE_WALL;
  tile_class[17] = TILE_FLOOR;
  tile_class[19] = TILE_FLOOR;
  tile_class[20] = TILE_FLOOR;
  tile_class[21] = TILE_FLOOR;
  //Menu Letters attributes 8x2
  menu_attrib[0] = game_paper | INK_GREEN | BRIGHT;
  menu_attrib[1] = game_paper | INK_GREEN ;
  menu_attrib[2] = game_paper | INK_CYAN  ;
  menu_attrib[3] = game_paper | INK_CYAN  | BRIGHT;
  game_attrib(INK_WHITE);

  game_border = INK_BLACK;
  game_ink = INK_GREEN;
  game_paper = PAPER_BLACK;

  game_score_top = 10000;

  /* Chimuelo Engine Main Loop */
  engine_init();
  while (1) {
    game_cls();
    // MENU
    if (game_menu) {
      menu_main();
    }
    // GAME
    game_cls();
    game_over = 0;
    game_loop();
    game_cls();
  }
}
