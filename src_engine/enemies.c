/*
        This file is part of PGD.

        PGD is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        PGD is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with PGD.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "game.h"
#include "audio.h"
#include "banks.h"
#include "enemies.h"
#include "engine.h"
#include "player.h"
#include "sprite.h"
#include "zx.h"
#include "macros.h"
#include <arch/zx.h>
#include <arch/zx/nirvana+.h>
#include <input.h>
#include <stdlib.h>

void enemy_turn(void) {


}

void enemy_draw() {
  // Draw Sprite (Move The sprite in Nirvana, for next frame)
  spr_paint();
  spr_back_repaint();
}

void enemy_move_horizontal(void) {

}

void enemy_move_ghost(void) {

}

void enemy_change_dir_v() {

}

void enemy_change_dir_h() {

}

void enemy_change_dir_fh() {

}

void enemy_change_dir_fv() {

}

void enemy_brain_h() {

}

void enemy_brain_v() {

}

void enemy_init(unsigned char f_class) {

  unsigned char f_tot_class;
  unsigned char f_basetile;
  unsigned int f_pos;

  unsigned char *f_init;

  // Found an empty sprite slot
  f_tot_class = 0;
  spr_index = 0;
  while (spr_index < GAME_MAX_ENEMIES) {
    if (sprite->spr_kind== E_NONE) {
      // found a record
      break;
    }
    ++spr_index;
  }

  f_init = (unsigned char *) &spr_init[0];

  // Search for Sprite Attribs on spr_init
  f_pos = 0;
  // Size of spr_init attributes
  while (f_pos <= (GAME_TOTAL_INDEX_CLASSES * GAME_SPR_INIT_SIZE)) {

    if (*(f_init + f_pos) == f_class) {

      // Read from ARRAY
      f_basetile = *(f_init + f_pos + 1);         // base tile
      sprite->spr_frames= *(f_init + f_pos + 2); // frames
      sprite->spr_altset= *(f_init + f_pos + 3);
      sprite->spr_kind= *(f_init + f_pos + 4); // Class of sprite
      sprite->spr_lin_inc= 2;
      // Read From Map
      sprite->class= f_class;
      game_scr_val();
      sprite->spr_speed= 1;
      // spr_speed= scr_map[index1 + 1];
      // value_a= scr_map[index1 + 32];
      // value_b= scr_map[index1 + 33];

      // Class Found!
      sprite->state= 0;

      // Position
      sprite->lin= (index1 / SCR_COLS) << 3; //*8
      sprite->col= index1 & 31;

      sprite->colint= 0;
      sprite->base_tile= f_basetile;
      sprite->tile= f_basetile;
      sprite->value_a= f_basetile;
      sprite->value_b= sprite->col;
      sprite->value_c= f_basetile;
      sprite->value_d= sprite->lin;
      sprite->value_e= sprite->col;
      enemy_custom(spr_index);

      ++spr_count;
      sprite->last_time= 0xFFFF; // zx_clock() - (rand() & 4) * 500;

      break;
    } else {
      // Next Class increment
      f_pos = f_pos + GAME_SPR_INIT_SIZE; // N variables on spr_init
    }
  }
}

void enemy_custom(unsigned char f_index) {
  sprite = sprites[f_index];
  switch (f_index) {
  case 0:
    sprite->lin = sprite->lin - 24;
    sprite->spr_kind = E_GHOST;
    break;

  case 2:
    sprite->col = sprite->col - 2;
    break;
  case 3:
    sprite->col = sprite->col + 2;
    break;
  }
}

void enemy_kill(unsigned char f_index) {
  sprite = sprites[f_index];
  NIRVANAP_spriteT(f_index, 0, 0, 0);
  NIRVANAP_spriteT(NIRV_SPRITE_P1, 0, 0, 0);
  NIRVANAP_fillT(PAPER_BLACK || INK_BLACK, sprite->lin, sprite->col);
  sprite = sprites[INDEX_P1];
  NIRVANAP_fillT(PAPER_BLACK || INK_BLACK, sprite->lin, sprite->col);

  NIRVANAP_halt();
  NIRVANAP_printQ(spr8_lookup_s[4 + player_eat_count],
                  spr8_lookup_e[4 + player_eat_count], sprite->lin,
                  sprite->col);
  NIRVANAP_printQ(spr8_lookup_s[8], spr8_lookup_e[8], sprite->lin,
                  sprite->col + 1);


  audio_eat_ghost();
  sprite = sprites[f_index];
  sprite->lin = (sprite->lin >> 3) << 3;
  sprite->colint = 0;
  sprite->spr_kind = E_EYES;
  sprite->value_a = 72;
  sprite->spr_frames = 1;
  sprite->spr_lin_inc = 8;
  sprite->spr_speed = ENEMY_FAST_SPEED;
  sprite->last_time = 0xFFFF;

  sprite = sprites[INDEX_P1];
  v0 = 20 << player_eat_count;
  if (!game_atrac)
    player_score_add(v0);
  ++player_eat_count;
}
