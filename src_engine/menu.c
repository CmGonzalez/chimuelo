/*
        This file is part of PGD.

        PGD is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        PGD is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with PGD.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "../game.h"
#include "../game_layout.h"
#include "../game_events.h"

#include "game.h"
#include "audio.h"
#include "banks.h"
#include "enemies.h"
#include "engine.h"
#include "player.h"
#include "sprite.h"
#include "zx.h"
#include "macros.h"
#include <arch/zx.h>
#include <arch/zx/nirvana+.h>
#include <intrinsic.h>
#include <stdlib.h>
#include <string.h>
#include <z80.h>

unsigned char menu_color;

void menu_main() {

  unsigned char f_input;
  unsigned char s_col;
  unsigned char s_col_e;
  unsigned char s_row;
  unsigned char c;
  unsigned int start_time;

  f_input = 1;
  s_col = 12;
  s_col_e = 12 + 9;

  c = 0;
  game_code = 0;
  game_paper = PAPER_BLACK;

  game_cls();
  menu_logo();
  audio_ay_menu();
  game_init();

  menu_main_print();
  i = 0;
  start_time = zx_clock();
  menu_color = 0;
  while (f_input) {
    z80_delay_ms(20);
    // in_wait_key();
    c = in_inkey();
    // in_wait_nokey();

    s_row = ((9 + menu_curr_sel) << 3) + 8;
    // ROTATE PAPER
    // v0 = menu_attrib[0];
    v0 = menu_color << 3;
    ++menu_color;
    if (menu_color == INK_CYAN) {
      ++menu_color;
    }
    if (menu_color > 6) {
      menu_color = 0;
    }

    menu_attrib[0] = (menu_attrib[0] & 0xC7) | v0;
    menu_attrib[1] = (menu_attrib[1] & 0xC7) | v0;
    menu_attrib[2] = (menu_attrib[2] & 0xC7) | v0;
    menu_attrib[3] = (menu_attrib[3] & 0xC7) | v0;
    game_paint_attrib(&menu_attrib, s_col + 1, s_col_e, s_row);
    // 48
    c = c - 48;
    switch (c) {
    case 1: // SINCLAIR
      joyfunc1 = (uint16_t(*)(udk_t *))(in_stick_sinclair1);
      menu_curr_sel = 1;
      break;
    case 2: // KEYBOARD
      joyfunc1 = (uint16_t(*)(udk_t *))(in_stick_keyboard);
      menu_curr_sel = 2;
      break;
    case 3: // KEMPSTON
      joyfunc1 = (uint16_t(*)(udk_t *))(in_stick_kempston);
      menu_curr_sel = 3;
      break;
    case 4: // DEFINE
      menu_redefine();
      joyfunc1 = (uint16_t(*)(udk_t *))(in_stick_keyboard);
      menu_curr_sel = 2;
      start_time = zx_clock();
      break;

    case 0:
      audio_eat_pill();
      game_cls();
      ay_reset();
      // audio_game_start();
      game_tune = 0; // default
      f_input = 0;   // Exit Loop
      break;
    }
    if (c > 0 && c < 5)
      game_paint_attrib(&attrib, s_col, s_col_e, s_row);
    ++i;

    if (game_check_time(&start_time, 250)) {
      game_atrac = 1;
      game_atrac_seq();
      start_time = zx_clock();
      zx_border(INK_BLACK);
      game_init();
      game_cls();
      menu_logo();
      menu_clear();
      menu_main_print();
      audio_ay_menu();
      game_atrac = 0;
    }
  }
}

void menu_main_print(void) {

  // intrinsic_halt();
  menu_clear();
  zx_print_str(10, 11, "1 SINCLAIR");
  zx_print_str(11, 11, "2 KEYBOARD");
  zx_print_str(12, 11, "3 KEMPSTON");
  zx_print_str(13, 11, "4 DEFINE");

  game_attrib(INK_MAGENTA);
  game_paint_attrib(&attrib, 0, 31, (16 << 3) + 8);
  zx_print_str(16, 11, "0 START");
  game_attrib(INK_YELLOW);
  zx_print_str(21, 8, "2019  NOENTIENDO");
  zx_print_ink(INK_BLUE);
  zx_print_str(23, 4, "ALFA4 - DON'T DISTRIBUTE!");
}

void menu_clear() {
  zx_print_ink(PAPER_BLACK | INK_WHITE | BRIGHT);
  for (v0 = 8; v0 < 23; ++v0) {
    game_fill_row(v0, 32);
    game_paint_attrib(&attrib, 0, 31, (v0 << 3) + 8);
  }
}

void menu_redefine() {

  menu_clear();
  menu_row = 11;
  zx_print_str(menu_row, 10, "PRESS A KEY");
  game_paint_attrib(&attrib, 0, 31, (menu_row << 3) + 8);
  menu_row++;
  menu_row++;
  zx_print_str(menu_row, 12, "LEFT");
  k1.left = menu_define_key();
  menu_row++;
  zx_print_str(menu_row, 12, "RIGHT");
  k1.right = menu_define_key();
  menu_row++;
  zx_print_str(menu_row, 12, "UP");
  k1.up = menu_define_key();
  menu_row++;
  zx_print_str(menu_row, 12, "DOWN");
  k1.down = menu_define_key();

  // k1.up = IN_KEY_SCANCODE_DISABLE;
  // k1.down = IN_KEY_SCANCODE_DISABLE;
  z80_delay_ms(255);
  // game_fill_row(12, 32);
  menu_main_print();
}

unsigned int menu_define_key() {

  while (1) {
    in_wait_key();
    v0 = in_inkey();
    v1 = v0;
    in_wait_nokey();
    if (v1 >= 61 && v1 <= 122) {
      v1 = v1 - 32; // TO UPPER
    }
    if ((v1 >= 30 && v1 <= 39) || (v1 >= 65 && v1 <= 90)) {
      zx_print_char(menu_row, 18, v1);
    }
    if (v1 == 13) {
      zx_print_str(menu_row, 18, "ENTER");
    }
    if (v1 == 32) {
      zx_print_str(menu_row, 18, "SPACE");
    }
    return in_key_scancode(v0);
  }
}

unsigned char menu_read_key(unsigned char row, unsigned char col) {
  unsigned char key;

  while (1) {
    in_wait_key();
    key = in_inkey();
    in_wait_nokey();
    // if (key >= 48 && key <=57) {
    // if (key >= 65 && key <= 90) {
    if (key >= 97 && key <= 122) {
      key = key - 32; // UPPERCASE
      zx_print_char(row, col, key);
      return key;
    }
  }
}

void menu_logo() {
  game_cls();

  v0 = 0;
  s_col1 = 0;
  s_lin1 = 24;
  while (v0 < 16) {
    NIRVANAP_drawT(104 + v0, s_lin1, s_col1 + 8);
    ++v0;
    s_col1 = s_col1 + 2;
    if (v0 == 8) {
      s_lin1 = s_lin1 + 16;
      s_col1 = 0;
    }
  }
}

void menu_rotcolor() {
  ++menu_color;
  if (menu_color > 7) {
    menu_color = 0;
  }
}

unsigned char game_atrac_seq() {
  return 0;
}

void menu_attrac() {
  /*
  unsigned int play_time;

  game_sprite_reset();
  player_init(s_lin1, 30, TILE_P1_RIGHT);
  scr_curr = 0;
  game_atrac = 2;
  game_round_init();
  play_time = zx_clock();
  col[INDEX_P1] = 15;
  lin[INDEX_P1] = 152;
  while (!game_check_time(&play_time, 2500) && !in_inkey()) {


    player_turn();
    enemy_turn();
    if (game_check_time(&time_key, 4)) {
      time_key = zx_clock();

      zx_print_ink((game_border << 3) | color);
      zx_print_str(23, 11, " NAM  CAP ");
      menu_rotcolor();
    }
  }
  */
}
