/*
        This file is part of PGD.

        PGD is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        PGD is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with PGD.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "../game.h"
#include "../game_layout.h"
#include "../game_events.h"

#include "audio.h"
#include "banks.h"
#include "enemies.h"
#include "engine.h"
#include "menu.h"
#include "player.h"
#include "sprite.h"
#include "zx.h"
#include "macros.h"

#include <arch/zx.h>
#include <arch/zx/nirvana+.h>
#include <intrinsic.h>
#include <stdlib.h>
#include <string.h>
#include <z80.h>
#include <compress/zx7.h>

void engine_init() {
    zx_border(game_border);
    zx_print_ink(game_ink);
    game_audio_mode = 128;
    game_song_play = 1;
    game_inmune = 0;    // GAME_INMUNE;
    game_inf_lives = 0; // GAME_INF_LIVES;
    // Nirvana Attributes lookup tables
    attribs = (unsigned char *)0xFCC8;
    deltas = (unsigned char *)0xFF01;
  #ifdef GAME_DEBUG
    z80_delay_ms(250);
    game_inmune = 0;    // GAME_INMUNE;
    game_inf_lives = 1; // GAME_INF_LIVES;
    game_menu = 0;
    menu_curr_sel = 2; // Sync Menu
    game_tune = 4;
  #else
    z80_delay_ms(666); // SATANIC DELAY
    game_menu = 1;
    menu_curr_sel = 1; // Sync Menu
  #endif
    // INTERRUPTS ARE DISABLED
    // RESET AY CHIP
    ay_reset();
    // Init Game
    game_start_timer();
    // ENABLE SOUND BASED ON DETECTED MODEL
    game_sound = spec128 ? (GAME_SOUND_AY_FX_ON | GAME_SOUND_AY_MUS_ON)
                         : (GAME_SOUND_48_FX_ON | GAME_SOUND_48_MUS_ON);
    // Keyboard Handling
    k1.fire = IN_KEY_SCANCODE_SPACE;//TODO DEBUG IN_KEY_SCANCODE_DISABLE;
    // TODO k1.fire1 = IN_KEY_SCANCODE_SPACE;
    k1.left = IN_KEY_SCANCODE_o;
    k1.right = IN_KEY_SCANCODE_p;
    // must be defined otherwise up is always true
    k1.up = IN_KEY_SCANCODE_q;
    k1.down = IN_KEY_SCANCODE_a;
  #ifdef GAME_DEBUG
    joyfunc1 = (uint16_t(*)(udk_t *))(in_stick_keyboard);
  #else
    joyfunc1 = (uint16_t(*)(udk_t *))(in_stick_sinclair1);
  #endif

    k2.left = IN_KEY_SCANCODE_CAPS;
    k2.right = IN_KEY_SCANCODE_SYM;

    k2.up = IN_KEY_SCANCODE_DISABLE;
    k2.down = IN_KEY_SCANCODE_DISABLE;
    k2.fire = IN_KEY_SCANCODE_DISABLE;
    joyfunc2 = (uint16_t(*)(udk_t *))(in_stick_keyboard);
#ifndef GAME_DEBUG
    for (loop_count = 31416; !in_test_key(); loop_count += 10061)
      ;
    srand(loop_count);
#endif
    // Clear Screen and init Nirvana
    zx_paper_fill(INK_BLACK | PAPER_BLACK);
    NIRVANAP_tiles(_btiles);
    spr_fill_lookup_table();
    NIRVANAP_start();

#ifndef SCR_OFFSET_X
  game_maxcol = SCR_COLS;
  game_mincol = 0;
#endif
#ifdef SCR_OFFSET_X
  game_maxcol = SCR_COLS + SCR_OFFSET_X;
  game_mincol = SCR_OFFSET_X;
#endif
  // TODO CUSTOMIZE - NIRVANA ONLY
  // ATTRIB NORMAL
  game_attrib(INK_YELLOW);

  game_over = 1;
  // Init Screen
  time_event = zx_clock();

}


/* Main Game Loop  */
void game_loop(void) {

  curr_time = 0;
  player_next_extra = 10000;
  //scr_curr = 0;
  player_lives = 3;
  player_score = 0;
  game_over = 0;
  game_round_up = 0;
  // scr_curr = 0xFF;
  game_paper = PAPER_BLACK;
  game_atrac = 0;

  game_round_init();
  NIRVANAP_halt();
  if (!game_atrac)


  /* Game Loop */
  loop_count = 0;
  while (!game_over) {
    while (!game_round_up && !game_over) {
      /* Enemies turn */
      enemy_turn();
      /* Player1 turn */
      player_turn();

      if (game_check_time(&time_event, GAME_EVENT)) {
        game_event();
#ifdef GAME_DEBUG
        game_fps();
#endif
        time_event = zx_clock();
      }
#ifdef GAME_DEBUG
      ++fps;
      zx_border(INK_BLACK);
#endif
      ++loop_count;
      if (sprite->state & BIT_DEAD) {
        // Player Killer
        //player_lost_life();
      }
    }
    game_round_up = 0;
    game_over = 0;
  }
}

void game_start_timer(void) {
  // Hook Timer Function to Nirvana Interrupts
  NIRVANAP_ISR_HOOK[0] = 205;                                // call
  z80_wpoke(&NIRVANAP_ISR_HOOK[1], (unsigned int)game_tick); // game_tick
}

void game_tick(void) {
  ++curr_time;
  zx_isr();
}

void game_sprite_restart(unsigned char f_index) {
  sprite = sprites[f_index];
  NIRVANAP_spriteT(f_index, 0, 0, 0);
  s_lin0 = sprite->lin;
  s_col0 = sprite->col;
  index0 = calc_index(s_lin0, s_col0);
  spr_back_repaint();
  sprite->lin = sprite->value_d;
  sprite->col = sprite->value_e;
  sprite->colint = 0;
  sprite->spr_kind = E_HORIZONTAL;
  sprite->spr_frames = 4;
  sprite->spr_lin_inc = 2;
  game_scr_val();
  sprite->spr_speed = 1;
  sprite->value_a = sprite->value_c;
  sprite->last_time = 0xFFFF; // zx_clock() - (rand() & 4) * 500;
  sprite->state = 0;
  enemy_custom(f_index);
}

#ifdef GAME_DEBUG
void game_fps(void) {
  zx_print_ink(INK_WHITE);
  zx_print_int(23, 16, fps);
  fps = 0;
}
#endif

void game_page_map(void) {
#ifdef MAP_BANK6
    intrinsic_di();
    page(6);
    dzx7_standard( &world0[lenght0[scr_curr]] , &scr_map[SCR_COLS*2]‬ ); // 2 fake rows for nirvana
    page(0);
    intrinsic_ei();
#endif
}

void page(unsigned char bank) {
  GLOBAL_ZX_PORT_7FFD = 0x10 + bank;
  IO_7FFD = 0x10 + bank;
}

void game_draw_map(void) {
  unsigned char val0;
  // Map colors
  game_paper = PAPER_BLACK; // btiles[32] & 0xF8;
  map_clear = game_paper | (game_paper >> 3);
  spr_count = 0;
  obj_count = 0;

  index1 = SCR_COLS; // Fake 1 rows for nirvana
  s_col1 = 0;
  s_lin1 = 8;
  // Game variables for loop reusage
  while (index1 < (SCR_COLS * SCR_ROWS) ) {
#ifdef MAP_OFFSET_TILE
    scr_map[index1] = scr_map[index1] - MAP_OFFSET_TILE;
#endif
    val0 = scr_map[index1];
    if (val0 < 32) {
      // Regular tile
#ifdef SCR_OFFSET_X
      NIRVANAP_printQ(spr8_lookup_s[val0], spr8_lookup_e[val0], s_lin1, s_col1 + SCR_OFFSET_X);
#endif
#ifndef SCR_OFFSET_X
      NIRVANAP_printQ(spr8_lookup_s[val0], spr8_lookup_e[val0], s_lin1, s_col1);
#endif
    }
    ++index1;
    ++s_col1;
    if (s_col1 >= SCR_COLS) {
      s_col1 = 0;
      s_lin1 = s_lin1 + 8;
    }
  }
#ifdef GAME_DEBUG
  debug_show = 0;
#endif
}

void game_round_init(void) {

  ay_reset();
  // All Black
  spr_clear_scr();
  game_scr_val();
  NIRVANAP_halt();
  NIRVANAP_stop();
  intrinsic_di();
  game_init();
  zx_set_clock(0);
  time_event = 0;
  player_coins = 0;
  // Draw Screen
  game_page_map();
  game_draw_map();

  zx_border(game_border);
  game_header();
#ifdef GAME_DEBUG
  fps = 0;
  zx_print_ink(INK_WHITE | PAPER_BLACK);
#endif
  intrinsic_ei();
  if (!game_over && !game_atrac) { // For Attract modes
    // Draw Player
    sprite = sprites[INDEX_P1];
    player_init(player_start_lin, player_start_col, player_start_tile);
    NIRVANAP_spriteT(NIRV_SPRITE_P1, sprite->tile + sprite->colint, sprite->lin, sprite->col);
    sprite->state = sprite->state & !BIT_DEAD;
    // Start Tune
    audio_ay_ingame();
  }
  NIRVANAP_start();
  game_footer();
}


void game_print_score(void) {
  zx_print_ink(INK_WHITE | BRIGHT);
  zx_print_paper(PAPER_BLACK);
  zx_print_int(0, 8, player_score);
  zx_print_int(0, 24, game_score_top); // SCORE TOP
}

void game_print_lives(void) {
  unsigned char f_col = 3;
  zx_print_ink(INK_YELLOW);
  if (player_lives <= 10) {
    v0 = 0;
    while (v0 < (player_lives - 1)) {
      zx_print_str(23, f_col + v0, "<");
      ++v0;
    }
  } else {
    zx_print_str(23, f_col, "<x");
    zx_print_chr(23, f_col + 2, player_lives);
  }
}


void game_end() {}

void game_cls() {
  NIRVANAP_halt();
  spr_clear_scr();
  zx_paper_fill(INK_BLACK | PAPER_BLACK);
  zx_border(INK_BLACK);
}

void game_paint_attrib(unsigned char *f_attrib[], char f_start,
                       unsigned char f_end, unsigned char f_lin) {

  unsigned char li;
  for (li = f_start; li < f_end; ++li) {
    NIRVANAP_paintC(f_attrib, f_lin, li);
  }
}

unsigned char game_check_time(unsigned int *start, unsigned int lapse) {
  if (zx_clock() - *start > lapse) {
    return 1;
  } else {
    return 0;
  }
}

void game_attrib(unsigned char f_color) {
  // ATTRIB NORMAL
  attrib[0] = game_paper | f_color;
  attrib[1] = game_paper | f_color | BRIGHT;
  attrib[2] = game_paper | INK_WHITE;
  attrib[3] = game_paper | f_color;
}


void zx_print_char(unsigned char ui_row, unsigned char ui_col,
                   unsigned char c) {
  unsigned char str[2];
  str[0] = c;
  str[1] = 0;
  zx_print_str(ui_row, ui_col, str);
}

void game_pause0() {
  unsigned char l;
  l = 0;
  while (l == 0) {
    l = in_test_key();
  }
  in_wait_nokey();
}


void game_sprite_reset() {
  /* code */
  game_paper = PAPER_BLACK;
  spr_count = 0;
  spr_index = 0;
  while (spr_index < GAME_MAX_ENEMIES) {
    sprite = sprites[spr_index];
    NIRVANAP_spriteT(spr_index, 0, 0, 0);
    sprite->spr_kind = E_NONE;
    sprite->col = 0;
    sprite->lin = 0;
    sprite->colint = 0;
    sprite->base_tile = 0;
    sprite->tile = 0;
    sprite->value_a = 0;
    sprite->value_b = 0;
    sprite->value_c = 0;
    sprite->value_d = 0;
    sprite->value_e = 0;
    ++spr_index;
  }
  index0 = 0;
  while (index0 < (SCR_COLS * SCR_ROWS) ) {
    scr_map[index0] = 0;
    ++index0;
  }
  sprite = sprites[INDEX_P1];
  sprite->spr_kind = E_NONE;
}

void game_scr_val() {
  if (scr_curr > GAME_MAX_VALUES) {
    v0 = 18;
  } else {
    v0 = scr_curr;
  }
}

unsigned int calc_index(unsigned char f_lin, unsigned char f_col) {
  // Mapeo a indice en scr_map 32x16 mapa
  // column + (line / 8) * 32
#ifdef SCR_OFFSET_X
  //SCR_COLS < 32
  return ( f_col - SCR_OFFSET_X ) + ( (f_lin >> 3) * SCR_COLS );
#endif
#ifndef SCR_OFFSET_X
  //SCR_COLS = 32
  return f_col + ((f_lin >> 3) << 5);
#endif

}
#ifdef GAME_DEBUG
void debug_start() {
  debug_show = 255;
  menu_attrib[0] = PAPER_BLACK | INK_YELLOW;
  menu_attrib[1] = PAPER_BLACK | INK_GREEN | BRIGHT;
  menu_attrib[2] = PAPER_BLACK | INK_CYAN;
  menu_attrib[3] = PAPER_BLACK | INK_BLUE | BRIGHT;

  v0=8;
  while(v0 <= SCR_LINS) {
    game_paint_attrib(&menu_attrib, 0, 6, v0);
    v0 = v0 + 8;
  }

  zx_print_str(1, 0, "COL");
  zx_print_str(2, 0, "INT");
  zx_print_str(3, 0, "LIN");
  zx_print_str(4, 0, "CST");
  zx_print_str(5, 0, "LST");


}

void debug_end() {
  menu_attrib[0] = PAPER_BLACK | INK_BLACK;
  menu_attrib[1] = PAPER_BLACK | INK_BLACK;
  menu_attrib[2] = PAPER_BLACK | INK_BLACK;
  menu_attrib[3] = PAPER_BLACK | INK_BLACK;
  v0=8;
  while(v0 <= SCR_LINS) {
    game_paint_attrib(&menu_attrib, 0, 6, v0);
    v0 = v0 + 8;
  }
}

void debug_messages() {

  v0 = in_inkey();
  switch (v0) {
  case 49: // Key 1
    --scr_curr;
    //game_round_up = 1;
    if (scr_curr == 255) {
      scr_curr =59;
    }
    in_wait_nokey();
    game_page_map();
    game_draw_map();
    break;
  case 50: // Key 2
    ++scr_curr;
    //game_round_up = 1;
    if (scr_curr == 60) {
      scr_curr = 0;
    }
    in_wait_nokey();
    game_page_map();
    game_draw_map();
    break;
  case 51: //Key 3
    debug_show = ~debug_show;
    if (debug_show) {
      debug_start();
    } else {
      debug_end();
    }

    break;
}


  if (debug_show) {
    sprite = sprites[INDEX_P1];
    zx_print_chr(1, 3, sprite->col);
    zx_print_chr(2, 3, sprite->colint);
    zx_print_chr(3, 3, sprite->lin);
    zx_print_chr(4, 3, sprite->state);
    zx_print_chr(5, 3, sprite->state_l);
  }


}
#endif

void plot(unsigned char x, unsigned char y)
{
   *zx_pxy2saddr(x,y) |= zx_px2bitmask(x);
}
