/*
        This file is part of PGD.

        PGD is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        PGD is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with PGD.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "game.h"
#include "banks.h"
#include "enemies.h"
#include "engine.h"
#include "player.h"
#include "sprite.h"
#include "zx.h"
#include "macros.h"
#include <arch/zx.h>
#include <arch/zx/nirvana+.h>
#include <intrinsic.h>
#include <stdlib.h>
#include <string.h>
#include <z80.h>


void ay_reset() {

  ay_reset_mfx();

}

void audio_beep_fx(unsigned char f_val) __z88dk_fastcall {
  __asm
      ld a, l;
      call _BeepFX_play
  __endasm;
  f_val = 0; /*to avoid z88dk warnings */
}

void audio_fx(unsigned char f_val) __z88dk_fastcall {
    __asm
        ld d, l;
        //ld d, 0x4;length / iterations
        loop_fx :
        add hl, de
        dec de
        ld a, h
        and 248
        out(0xfe), a
        ld a, e
        or d
        jr nz, loop_fx
    __endasm;
    f_val = 0; /*to avoid z88dk warnings */
}

void audio_fx2(unsigned char f_val) __z88dk_fastcall {
    __asm
    ld c, l;
    lp1:
      inc hl
      ld a,(hl)
      and 248

      out (254),a
      ld b,c
    lp0:
    	djnz lp0
      dec c
      jr nz,lp1
    ret
    __endasm;
    f_val = 0; /*to avoid z88dk warnings */
}


void audio_ay_ingame(void) {
  //ay_song_play(AY_SONG_LOOP, 4, ay_song_ingame0);
}

void audio_ay_menu(void) {
  //ay_song_play(AY_SONG_LOOP, 4, ay_song_ingame0);
}

void audio_eat_dot(void) {

}

void audio_add_fruit() {

  switch (game_audio_mode) {
  case 128:
    ay_fx_play_mfx(4, ay_fx_fruit_target);
    break;
  case 48:
    audio_fx(7);
    audio_fx2(128);
    break;
  }
}

void audio_eat_fruit() {

  switch (game_audio_mode) {
  case 128:
    ay_fx_play_mfx(4, ay_fx_fruit_eat);
    break;
  case 48:
    audio_beep_fx(1);
    break;
  }
}

void audio_eat_pill() {
  switch (game_audio_mode) {
  case 128:
    ay_fx_play_mfx(4, ay_fx_fruit_eat);
    break;
  case 48:
    audio_fx(4);
    break;
  }

}

void audio_eat_ghost() {
  switch (game_audio_mode) {
  case 128:
    ay_fx_play_mfx(4, ay_fx_ghost_eat);
    z80_delay_ms(125);
    break;
  case 48:
    audio_beep_fx(0);
    break;
  }

}

void audio_ay_death() {
  if (game_audio_mode == 128) {
    ay_fx_play_mfx(4, ay_fx_death);
  }
}
void audio_beep_death1() {
  if (game_audio_mode == 48) {
    audio_fx(7);
  }
}

void audio_beep_death2() {
  if (game_audio_mode == 48) {
    audio_beep_fx(3);
    z80_delay_ms(25);
    audio_beep_fx(3);
  }
}

void audio_fall() {

}

void audio_steve(unsigned char fx) {
  fx = 0; /*to avoid z88dk warnings */
/*
__asm
ld a, l            ;
    ;//z88dk passes the L register from unsigned char


    ld (sonreq),a    ;
    ;//32991 shows 0
    ;//ld (sonnow),a//added

play_sound:
    call sound        ;
    ld a,(sonnow)    ;
    or a            ;
    jr nz, play_sound;
    ret ;return back to C

sound:
        ld a,(sonreq) ;play new sound? ;32777
        ;//loads contents of 32991 into register A
        ; 32991 shows 0


        and a    ;32780
        jr z,nonew  ;NO
        ;jumps to nonew

;YES
        ld (sonnow),a
        dec a
        jr z,noise    ;#01 noise

        ld hl,sfxData
        dec a
        add a,a
        add a,a
        add a,a
        ld e,a
        xor a
        ld (sonreq),a
        ld d,a
        add hl,de
        ld bc, 8
        ld de,sonfrq ;33245    <--start freq
        ldir        ;32809
        jr process

    nonew:
        ld a,(sonnow);32813

        ;//and the old sound is?
        ;//loads contents of 32990 into register A

        and a
        ret z

        dec a    ;//continue noise?
        jr nz,process ;//continue sound

        jr cnois

    noise:
        ld a,0xA
        ld (sonlen),a
        xor a
        ld (sonreq),a

    cnois:
        ld b,0x30

    gain:
        call random
        and 0x10
        out (0xfe),a
        ld c,2

    make:
        dec c
        jr nz,make
        djnz gain
        ld hl,sonlen
        dec (hl)
        ret nz
        xor a
        ld (sonnow),a
        ret

    process:
        ld a,(sonfrq)    ;<--start freq
        ld h,a
        ld a,0x10
        ld d,0xff

    sonlp:
        ld e,h
        out (0xfe),a
        xor 0x10

    freq:
        dec d
        jr z,mod
        dec e
        jr nz,freq
        jr sonlp

    mod:
        ld a,(soncfg)
        add a,h
        ld (sonfrq),a ;        <--start freq
        ld hl,sonmod
        dec (hl)
        ret nz
        ld hl,sonlen
        dec (hl)
        jr nz,modify
        xor a
        ld (sonnow),a
        ld a,(sonnex)
        and a
        ret z
        ld (sonreq),a
        ret

    modify:
        ld a,(sobrsf)
        ld c,a
        ld a,(sontyp)
        and a
        jr z,reset
        dec a
        jr z,typ1
        dec a
        jr z,typ2

    typoth:
        ld a,(soncfg)
        neg
        ld (soncfg),a
        jr mode

    typ2:
        inc c
        inc c
        ld a,c
        ld (sobrsf),a
        jr reset

    typ1:
        dec c
        dec c
        ld a,c
        ld (sobrsf),a
        jr reset

    reset:
        ld a,c
        ld (sonfrq),a    ;<--start freq

    mode:
        ld a,(sonrnd)
        ld (sonmod),a
        ret

    random:
        push hl
        ld hl,(rnseed)
        inc hl
        ld a,h
        and 3
        ld h,a

    rok:
        ld (rnseed),a
        ld a,r
        xor (hl)
        pop hl
        ret

rnseed:    defw 0x1000

sonfrq:    defb sfxData    ;start frequency
soncfg:    defb sfxData+1    ;frequency change
sonmod:    defb sfxData+2    ;change times
sonlen:    defb sfxData+3    ;repeat times
sontyp:    defb sfxData+4    ;modulate type
                ;0 sawtooth
                ;1 2nd mod down
                ;2 2nd mod up
                ;3+ triangle
sobrsf:    defb sfxData+5    ;reset frequency
sonrnd:    defb sfxData+6    ;change reset temp
sonnex:    defb sfxData+7    ;linked sfx

sonnow:    defb 0    ;which effect playing
sonreq:    defb 0    ;we have an effect


;here all sounds excepts number 1 reserved for random noise
sfxData:
defb 0, 5, 5, 1, 0, 0, 0, 0;SFX 2
defb 40, 5, 10, 1, 0, 0, 0, 0;SFX 3
defb 0, 128, 30, 1, 0, 0, 0, 0;SFX 4
defb 0, 2, 30, 1, 0, 0, 0, 0;SFX 5
defb 0, 125, 32, 1, 0, 0, 0, 0;SFX 6
defb 255, 131, 32, 1, 0, 0, 0, 0;SFX 7
defb 255, 131, 40, 32, 1, 60, 1, 0;SFX 8
defb 240, 240, 8, 3, 0, 60, 6, 0;SFX 9
defb 2, 128, 10, 1, 0, 0, 0, 0;SFX 10
defb 40, 250, 8, 1, 0, 0, 0, 0;SFX 11
defb 250, 44, 6, 10, 1, 90, 1, 0;SFX 12
defb 0, 252, 20, 8, 1, 80, 8, 0;SFX 13
defb 230, 230, 4, 1, 1, 0, 0, 0;SFX 14
defb 45, 67, 20, 1, 1, 0, 0, 0;SFX 15
defb 64,  5,  10,  1,  0,  0,  0,  0;SFX 16
defb 40,  5,  10,  1,  0,  0,  0,  0;SFX 17
defb 64,  5,  20,  1,  0,  0,  0,  0;SFX 18
defb 20,220,  64,  1,  3,  10, 1,  0;SFX 19
defb 64,  4,  32,  1,  3,  96,  0,  2;SFX 20
defb 255,131, 32, 1, 0, 0, 0, 0;SFX 21
defb 240, 240, 8, 3, 0, 60, 6, 0;SFX 22
defb 2,120, 10, 1, 0, 0, 0, 0;SFX 23
defb 40,250, 8, 1, 0, 0, 0, 0;SFX 24
defb 250, 44, 6, 10, 1, 132, 1, 0;SFX 25
defb 0, 252, 20, 8, 1, 80, 8, 0;SFX 26
defb 230,230, 4, 1, 1, 0, 0, 0;SFX 27



__endasm;
*/
}
