#include "globals.h"
#include <arch/zx.h>
#ifdef MAP_BANK6
#include "../game_map.h"
#endif


/* This Data is placed on Bank #6
/ zsdcc can only place code and rodata into different banks at the moment
/ anything else use assembly language and sections
/ Compressed MAP Data for game paging
*/

// ----------------------------------------------------------------------------
// Decompress (from source to destination address) data that was previously
// compressed using ZX7. This is the smallest version of the ZX7 decompressor.
//
// Parameters:
//     src: source address (compressed data)
//     dst: destination address (decompressing)
// ----------------------------------------------------------------------------
 void dzx7Standard(unsigned char *src, unsigned char *dst) {

 }
