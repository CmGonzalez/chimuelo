;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; IM2 INTERRUPT SERVICE ROUTINE ;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SECTION code_crt_common

PUBLIC _zx_isr
EXTERN _spec128, _GLOBAL_ZX_PORT_7FFD

_zx_isr:
INCLUDE "banks/src/mfx_isr.asm"
;INCLUDE "banks/src/voicenc_midi_isr.asm"
ret


INCLUDE "banks/src/print_utils.asm"
INCLUDE "banks/src/BeepFX.asm"
