/*
        This file is part of PGD.

        PGD is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        PGD is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with PGD.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "../game.h"
#include "../globals.h"
#include "../game_layout.h"
#include "../game_events.h"

#include "audio.h"
#include "banks.h"
#include "enemies.h"
#include "engine.h"
#include "player.h"
#include "sprite.h"
#include "zx.h"
#include "macros.h"
#include <arch/zx.h>
#include <arch/zx/nirvana+.h>
#include <input.h>
#include <stdlib.h>



unsigned char spr_chktime(void) {
  if (zx_clock() - sprite->last_time >= sprite->spr_speed) {
    sprite->last_time = zx_clock();
    return 1;
  }
  return 0;
}

unsigned char spr_checkmap(unsigned int f_index) {
  if (tile_class[scr_map[f_index]] == TILE_WALL) {
    return 0;
  } else {
    return 1;
  }
}

unsigned char spr_checkmap_down(unsigned int f_index) {
  v0 = tile_class[scr_map[f_index]];

  switch (tile_class[scr_map[f_index]]) {
    case TILE_WALL:
      return 0;
    break;
    case TILE_FLOOR:
#ifdef GAME_JUMP
      if (spr_index == INDEX_P1) {
        if ( s_lin0+16  > ( (f_index/SCR_COLS) << 3) ) {
          return 1;
        } else {
          return 0;
        }
      }
#endif
      return 0;
    break;
    default:
      return 1;
    break;
  }

}

unsigned char spr_cango_up() {
  index1 = calc_index(s_lin0 - sprite->spr_lin_inc, s_col0);
//  v0 = 0; //TODO z88dk BUG!
  return spr_checkmap(index1) && spr_checkmap(index1 + 1);
}

unsigned char spr_cango_down() {
    unsigned int f_int;//TODO BUG? I WAS USING index1 and geting strange values!
    f_int = calc_index(s_lin0 + 16, s_col0);
    if (f_int < (SCR_COLS * SCR_ROWS)) {
      return spr_checkmap_down(f_int) && spr_checkmap_down(f_int+1);
    } else {
      return 1;
    }
}

unsigned char spr_cango_left() {
  if (sprite->colint > 0) {
    return 1;
  }
#ifndef SCR_OFFSET_X
  if (s_col0 > 0) {
    index1 = calc_index(s_lin0, s_col0 - 1);
    return spr_checkmap(index1) && spr_checkmap(index1 + SCR_COLS);
  } else {
    return 1;
  }
#endif
#ifdef SCR_OFFSET_X
  if (s_col0 > SCR_OFFSET_X) {
    index1 = calc_index(s_lin0, s_col0 - 1);
    return spr_checkmap(index1) && spr_checkmap(index1 + SCR_COLS);
  } else {
    return 1;
  }
#endif

}

unsigned char spr_cango_right() {
  if (sprite->colint < (sprite->spr_frames - 1) )  {
    return 1;
  }
#ifndef SCR_OFFSET_X
  /* 32 - 2 */
  if (s_col0 < 30) {
    index1 = calc_index(s_lin0, s_col0 + 2);
    return spr_checkmap(index1) && spr_checkmap(index1 + SCR_COLS);
  } else {
    return 1;
  }
#endif
#ifdef SCR_OFFSET_X
  if (s_col0 < SCR_OFFSET_X + SCR_COLS - 2) {
    index1 = calc_index(s_lin0, s_col0 + 2);
    return spr_checkmap(index1) && spr_checkmap(index1 + SCR_COLS);
  } else {
    return 1;
  }
#endif
}

void spr_set_up() {
#ifdef GAME_MAZE
  if (sprite->state & BIT_HORIZ) {
    sprite->colint = 0;
  }
#endif
  sprite->state &= ~BIT_DOWN;
  sprite->state |= BIT_UP;
}

void spr_set_down() {
#ifdef GAME_MAZE
  if (sprite->state & BIT_HORIZ) {
    sprite->colint = 3;
  }
#endif
  sprite->state &= ~BIT_UP;
  sprite->state |= BIT_DOWN;
}

void spr_set_left() {
#ifdef GAME_MAZE
  if (sprite->state & BIT_VERT) {
    sprite->colint = 3;
  }
#endif
  sprite->state_l = sprite->state;
  sprite->state &= ~BIT_RIGHT;
  sprite->state |= BIT_LEFT;
  spr_set_tiledir();
}

void spr_set_right() {
#ifdef GAME_MAZE
  if (sprite->state & BIT_VERT) {
    sprite->colint = 3;
  }
#endif
  sprite->state_l = sprite->state;
  sprite->state &= ~BIT_LEFT;
  sprite->state |= BIT_RIGHT;
  spr_set_tiledir();
}

unsigned char spr_move_up(void) {

  if (sprite->vert_anim) {
    ++sprite->colint;
    if (sprite->colint >= sprite->spr_frames) {
      sprite->colint = 0;
    };
  }
  v0 = sprite->lin - sprite->spr_lin_inc;
  if ( v0 <= SCR_LINS ) { // Overflow
    sprite->lin = sprite->lin - sprite->spr_lin_inc;
  } else {
#ifdef SCR_LOOP_Y
    NIRVANAP_spriteT(spr_index, 0, 0, 0);
    NIRVANAP_halt();
    spr_back_repaint();
    sprite->lin = SCR_LINS - 16;
#endif
  }

  return 0;
}

unsigned char spr_move_down(void) {

  if (sprite->vert_anim) {
    ++sprite->colint;
    if (sprite->colint >= sprite->spr_frames) {
      sprite->colint = 0;
    };
  }

  if ( sprite->lin < SCR_LINS ) {
    sprite->lin = sprite->lin + sprite->spr_lin_inc;
  } else {
#ifdef SCR_LOOP_Y
          NIRVANAP_spriteT(spr_index, 0, 0, 0);
          NIRVANAP_halt();
          spr_back_repaint();
          sprite->lin = 0;
#endif
  }
  return 0;
}

unsigned char spr_move_left(void) {
  --sprite->colint;
  if (sprite->colint >= sprite->spr_frames) {

    --sprite->col;

    if (sprite->col < game_mincol) {
#ifdef SCR_LOOP_X
      NIRVANAP_spriteT(spr_index, 0, 0, 0);
      NIRVANAP_halt();
      spr_back_repaint();
      s_col0 = game_maxcol - 2;
      sprite->col = s_col0;
#endif
    }
    sprite->colint = sprite->spr_frames - 1;

    return 1;
  }
  return 0;
}

unsigned char spr_move_right(void) {

  ++sprite->colint;
  if (sprite->colint >= sprite->spr_frames) {
    ++sprite->col;
    if (sprite->col > game_maxcol - 2 ) {
#ifdef SCR_LOOP_X
      NIRVANAP_spriteT(spr_index, 0, 0, 0);
      NIRVANAP_halt();
      spr_back_repaint();
      s_col0 = game_mincol;
      sprite->col = s_col0;
#endif
    }
    sprite->colint = 0;
    return 1;
  }
  return 0;
}

void spr_move_horizontal() {
  if ( (sprite->state & BIT_LEFT) && spr_cango_left()) {
    spr_move_left();
  }
  if ( (sprite->state & BIT_RIGHT) && spr_cango_right()) {
    spr_move_right();
  }
}

#ifdef GAME_JUMP
void spr_new_jump(void) {
  sprite->state &= ~BIT_UP;
  sprite->state &= ~BIT_DOWN;
  sprite->state |= BIT_JUMP;
  player_jump_count = 0;
  player_jump_lin = s_lin0;
  //audio_jump();
  //spr_set_up();
}

void spr_end_jump(void) {

  sprite->state &= ~BIT_JUMP;
  //state &= ~BIT_HORIZ;
  sprite->state |= BIT_DOWN;
  sprite->spr_speed = PLAYER_SPEED;
  player_jump_count = 0;
  spr_set_tiledir();
}

unsigned char spr_move_jump(void) {
    signed char lin_c;

    lin_c = player_jump_vert[player_jump_count];
    if (lin_c < 0) {
      //Up
      if (spr_cango_jump(lin_c)) {
        sprite->lin = sprite->lin + lin_c;
        if (sprite->lin > SCR_LINS) sprite->lin = SCR_LINS;
      } else {
        spr_end_jump();
      }
    }
    if (lin_c > 0) {
      //Down
      if (spr_cango_fall(lin_c)) {
        sprite->lin = sprite->lin + lin_c;
        if (sprite->lin > SCR_LINS) sprite->lin = 0;
      } else {
        spr_end_jump();
      }
    }

    spr_move_horizontal();
    ++player_jump_count;
    if (player_jump_count >= GAME_JUMP_HOR) {
      // CAE COMO ROCA
      spr_end_jump();
    }
    return 1;
}

unsigned char spr_cango_jump(signed char f_inc) {
  index1 = calc_index(s_lin0 + f_inc, s_col0);
  v0 = 0; //TODO z88dk BUG!
  return spr_checkmap(index1) && spr_checkmap(index1 + 1);
}

unsigned char spr_cango_fall(signed char f_inc) {
    unsigned int f_int;
    f_int = calc_index(s_lin0 + 14 + f_inc, s_col0); //14! should be 16!
    if (f_int < (SCR_COLS * SCR_ROWS)) {
      return spr_checkmap_down(f_int) && spr_checkmap_down(f_int + 1);
    } else {
      return 1;
    }
}
#endif

void spr_clear_scr() {
  unsigned char i;
  unsigned char j;
  zx_print_ink(PAPER_BLACK | INK_BLACK);
  // Clear Sprites
  for (i = 0; i < NIRV_TOTAL_SPRITES; ++i) {
    NIRVANAP_spriteT(i, TILE_EMPTY, 0, 0);
  }

  for (i = 0; i < 8; ++i) {

    j = 0;
    v0 = i << 1;
    zx_print_str(0,v0,"  ");
    while (j <= SCR_LINS) {
      NIRVANAP_fillT(INK_BLACK || PAPER_BLACK, j, v0);
      j = j + 16;
    }
    zx_print_str(23,v0,"  ");
    j = 0;
    v0 = 30 - (i << 1);
    zx_print_str(0,v0,"  ");
    while (j <= SCR_LINS) {
      NIRVANAP_fillT(INK_BLACK || PAPER_BLACK, j, v0);
      j = j + 16;
    }
    zx_print_str(23,v0,"  ");
    NIRVANAP_halt();

  }

  //game_fill_row(23, 22);
}

void spr_paint(void) {
  NIRVANAP_spriteT(spr_index, sprite->tile + sprite->colint, sprite->lin,
                   sprite->col);
}


unsigned char spr_set_tiledir(void) {
  // Search enemy class associated Values
  unsigned char l_state;

  if ((sprite->state & BIT_RIGHT) || (sprite->state & BIT_LEFT)) { //TODO BIT HORIZONTAL
    l_state = sprite->state;
  } else {
    l_state = sprite->state_l;
  }

  if (l_state & BIT_RIGHT) {
    sprite->tile = sprite->base_tile;
    return sprite->tile;
  }
  if (l_state & BIT_LEFT) {
    sprite->tile = sprite->base_tile + sprite->spr_frames;
    return sprite->tile;
  }
#ifdef GAME_MAZE
//Maze only pacman type animations 4 directions
  if (l_state & BIT_UP) {
    sprite->tile = sprite->base_tile + sprite->spr_frames * 2;
    return sprite->tile;
  }
  if (l_state & BIT_DOWN) {
    sprite->tile = sprite->base_tile + sprite->spr_frames * 3;
    return sprite->tile;
  }
#endif
  return 0;
}

void spr_back_repaint(void) {
  unsigned int f_index;
  unsigned char f_lin;
  unsigned char f_tile;
  // Repaints the Backgound by reading back from scr_map the needed tile_class
  // We paint 4 tiles if the sprite is on 8 multiples coordinates
  // if the sprite is not on 8 multiple line, We paint 2 extra tiles to avoid
  // corruption

  f_index = index0;
  f_lin = (s_lin0 >> 3) << 3;
  s_col1 = s_col0 + 1;
  // UP RIGHT
  // spr_draw8(scr_map[f_index], f_lin, s_col0);
  f_tile = scr_map[f_index];
  NIRVANAP_printQ(spr8_lookup_s[f_tile], spr8_lookup_e[f_tile], f_lin, s_col0);

  // UP Left
  // spr_draw8(scr_map[f_index++], f_lin, s_col1);
  f_index++;
  f_tile = scr_map[f_index];
  NIRVANAP_printQ(spr8_lookup_s[f_tile], spr8_lookup_e[f_tile], f_lin, s_col1);

  // DOWN RIGHT
  f_lin = f_lin + 8;
  f_index = f_index + SCR_COLS - 1;
  f_tile = scr_map[f_index];
  // spr_draw8(scr_map[f_index = +31], f_lin, s_col0);
  NIRVANAP_printQ(spr8_lookup_s[f_tile], spr8_lookup_e[f_tile], f_lin, s_col0);

  // DOWN LEFT
  // spr_draw8(scr_map[f_index++], f_lin, s_col1);
  f_index++;
  f_tile = scr_map[f_index];
  NIRVANAP_printQ(spr8_lookup_s[f_tile], spr8_lookup_e[f_tile], f_lin, s_col1);

  if ( (s_lin0 & 7) != 0 ) { // Modulo 8
    // DOWN RIGHT
    f_lin = f_lin + 8;
    f_index = f_index + SCR_COLS - 1;
    f_tile = scr_map[f_index];
    // spr_draw8(scr_map[f_index = +31], f_lin, s_col0);
    NIRVANAP_printQ(spr8_lookup_s[f_tile], spr8_lookup_e[f_tile], f_lin, s_col0);

    // DOWN LEFT
    // spr_draw8(scr_map[f_index++], f_lin, s_col1);
    f_index++;
    f_tile = scr_map[f_index];
    NIRVANAP_printQ(spr8_lookup_s[f_tile], spr8_lookup_e[f_tile], f_lin, s_col1);
  }

}

void spr_fill_lookup_table() {
  v0 = 0;
  while (v0 < GAME_MAX_TILES) {

    gbyte = &btiles[0] + (48 * (v0 >> 2));
    switch (v0 & 3) {
    case 0:
    //zx_print_int(23,0,(unsigned int) gbyte);
      spr8_lookup_s[v0] = (unsigned int) gbyte; //TODO z88dk bug? por que el cast
      spr8_lookup_e[v0] = gbyte + 32;
      break;
    case 1:
      spr8_lookup_s[v0] = gbyte + 1;
      spr8_lookup_e[v0] = gbyte + 40;
      break;
    case 2:
      spr8_lookup_s[v0] = gbyte + 16;
      spr8_lookup_e[v0] = gbyte + 36;
      break;
    case 3:
      spr8_lookup_s[v0] = gbyte + 17;
      spr8_lookup_e[v0] = gbyte + 44;
      break;
    }

    ++v0;
  }
}
