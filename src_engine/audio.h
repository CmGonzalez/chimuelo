/*
	This file is part of PGD.

	PGD is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	PGD is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with PGD.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef GAME_AUDIO_H
#define GAME_AUDIO_H

#include "globals.h"

extern void ay_reset();

extern void audio_ay_ingame();
extern void audio_ay_menu();

extern void audio_eat_dot();
extern void audio_eat_pill();
extern void audio_eat_ghost();


extern void audio_ay_death();
extern void audio_beep_death1();
extern void audio_beep_death2();




extern void audio_beep_fx(unsigned char) __z88dk_fastcall;
extern void audio_fx(unsigned char) __z88dk_fastcall;
extern void audio_fx2(unsigned char) __z88dk_fastcall;

extern void audio_add_fruit();
extern void audio_eat_fruit();

#ifdef GAME_GRAVITY
extern void audio_fall();
#endif

#endif
