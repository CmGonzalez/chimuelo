/*
        This file is part of PGD.

        PGD is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        PGD is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with PGD.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "../game.h"
#include "../globals.h"
#include "../game_layout.h"
#include "../game_events.h"

#include "audio.h"
#include "banks.h"
#include "enemies.h"
#include "engine.h"
#include "player.h"
#include "sprite.h"
#include "zx.h"
#include "macros.h"

#include <arch/zx.h>
#include <arch/zx/nirvana+.h>
#include <input.h>
#include <stdlib.h>

void player_turn(void) {

  spr_index =  INDEX_P1;
  sprite = sprites[spr_index];

  if (spr_chktime()) {
#ifdef GAME_DEBUG
    zx_border(INK_BLACK);
#endif
    s_lin0 = sprite->lin;
    s_col0 = sprite->col;
    s_colint0 = sprite->colint;
    index0 = calc_index(s_lin0, s_col0);
    /* Read Input */

    if (!game_atrac) player_read_input();
    /* User Defined Player Movemente Call */
#ifdef GAME_GRAVITY
    player_gravity();
#endif

    player_move();

    /* Draw to Nirvana Sprites */
    NIRVANAP_spriteT(NIRV_SPRITE_P1, sprite->tile + sprite->colint,
                     sprite->lin, sprite->col);

    /* Repaint Background */
    spr_back_repaint();
    /* Player Collision */
    player_collision();
    if ((s_lin0 & 7) == 0) {
      player_pick_item(0);
      player_pick_item(1);
      player_pick_item(32);
      player_pick_item(33);
    }

// Magic Keys
#ifdef GAME_DEBUG
  debug_messages();
#endif
  }
}

void player_init(unsigned char f_lin, unsigned char f_col,
                 unsigned char f_tile) {
  // COMMON SPRITE VARIABLES
  spr_index = INDEX_P1;
  sprite = sprites[INDEX_P1];
  sprite->spr_kind = E_PLAYER;
  sprite->spr_speed = PLAYER_SPEED;
  sprite->spr_frames = 4;
  sprite->spr_lin_inc = 2;

  sprite->lin = f_lin; //*SPRITELIN(INDEX_P1);
  sprite->col = f_col; //*SPRITECOL(INDEX_P1);
zx_print_chr(0,0,sprite->col);
  player_jump_top = f_lin;
  sprite->state = 0;
  sprite->last_time = zx_clock();

  // PLAYER ONLY VARIABLES
  sprite->base_tile = f_tile;
  if (sprite->value_a) {
    sprite->colint = 3;
    sprite->state = sprite->state | BIT_LEFT;
    sprite->colint = sprite->value_b;
  } else {
    sprite->state = sprite->state | BIT_RIGHT;
    sprite->colint = sprite->value_b;
  }
  sprite->value_d = sprite->lin;
  sprite->value_e = sprite->col;
  spr_set_tiledir();
}



#ifdef GAME_GRAVITY

void player_gravity() {
  /* Check if the player have floor, and set fall if not */
  if ( !(sprite->state & BIT_JUMP) ) {
    if ( spr_cango_down() ) {
      sprite->state |= BIT_DOWN;
      player_jump_top = sprite->lin;
      player_jump_count = 0xFF;
      audio_fall();
    } else {
      sprite->state &= ~BIT_DOWN;
    }
  }
}
#endif

unsigned char player_read_input() {
  return (joyfunc1)(&k1);
}

void player_collision() {
  /*
  // Left
  unsigned char f_index;

  // Sprite Collision
  f_index = 0;
  while (f_index < spr_count) {
    switch (spr_kind[f_index]) {
    case E_GHOST:
      if ((abs(col[f_index] - col[INDEX_P1]) <= 1) &&
          (abs(lin[f_index] - lin[INDEX_P1]) < 16))
        player_kill();
      break;
    case E_WIMP:
      if ((abs(col[f_index] - col[INDEX_P1]) <= 1) &&
          (abs(lin[f_index] - lin[INDEX_P1]) < 16))
        enemy_kill(f_index);
      break;
    case E_WIMP_ALT:
      if ((abs(col[f_index] - col[INDEX_P1]) <= 1) &&
          (abs(lin[f_index] - lin[INDEX_P1]) < 16))
        enemy_kill(f_index);
      break;
    case E_PICKEABLE: //TODO PICKEABLE SPRITES EJ. MS.Pac Man Fruits
      if ((abs(col[f_index] - col[INDEX_P1]) <= 1) &&
          (abs(lin[f_index] - lin[INDEX_P1]) < 16)) {
        player_pick_spr();
      }

      break;
    }
    ++f_index;
  }
  */
}

void player_kill() {
  sprite->state = sprite->state & !BIT_DEAD;
}

unsigned char player_check_input(void) {

  if ((dirs & IN_STICK_LEFT) && (dirs & IN_STICK_RIGHT)) {
    // Error on reading both horizontal's
    dirs = 0;
  }
  return 0;
}

void player_score_add(unsigned int f_score) __z88dk_fastcall {
  if (!game_atrac) {
    player_score = player_score + f_score;

    // CHECK FOR TOP SCORE
    if (player_score > game_score_top) {
      game_score_top = player_score;
    }

    // CHECK FOR EXTRA life
    if (player_score > player_next_extra) {
      if (player_lives < 255) {
        player_lives = player_lives + 1;
      }

      player_next_extra = player_next_extra + 10000;
    }
    // ROTATE HIGH SCORE
    if (player_score > 1000000) {
      player_score = 0;
      player_next_extra = 10000;
    }

    game_print_score();
  }
}

void player_pick_item(unsigned char l_val) __z88dk_fastcall {

}

void player_lost_life() {
  /*
  --player_lives;
  game_sprite_restart(0);
  game_sprite_restart(1);
  game_sprite_restart(2);
  game_sprite_restart(3);
  NIRVANAP_spriteT(NIRV_SPRITE_FG, 0, 0, 0);
  NIRVANAP_halt();
  NIRVANAP_fillT(PAPER_BLACK | INK_BLACK, lin[INDEX_FRUIT], col[INDEX_FRUIT]);
  audio_ay_death();
  i = 0;
  while (i < 10) {
    NIRVANAP_spriteT(NIRV_SPRITE_P1, 80 + i, lin[INDEX_P1], col[INDEX_P1]);
    ++i;
    if ((i & 1) == 0) {
      audio_beep_death1();
      if (game_audio_mode == 128) z80_delay_ms(50);
    } else {
      z80_delay_ms(5);
    }
  }
  audio_beep_death2();
  NIRVANAP_fillT(PAPER_BLACK | INK_BLACK, lin[INDEX_P1], col[INDEX_P1]);
  NIRVANAP_spriteT(NIRV_SPRITE_P1, 0, 0, 0);
  state[INDEX_P1] = 0;
  lin[INDEX_P1] = value_d[INDEX_P1];
  col[INDEX_P1] = value_e[INDEX_P1];
  // Fruit Clear
  NIRVANAP_spriteT(NIRV_SPRITE_FG, 0, 0, 0);
  spr_kind[INDEX_FRUIT] = E_NONE;
  last_time[INDEX_FRUIT] = zx_clock();
  z80_delay_ms(100);
  loop_count = 0;
  game_footer();
  if (player_lives == 0) {
    game_over = 1;
  }
  */
}
