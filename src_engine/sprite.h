/*
	This file is part of PGD.

	PGD is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	PGD is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with PGD.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef GAME_SPRITE_H
#define GAME_SPRITE_H
extern unsigned char    spr_chktime(void);

extern unsigned char    spr_cango_up();
extern unsigned char    spr_cango_down();
extern unsigned char    spr_cango_left();
extern unsigned char    spr_cango_right();
extern unsigned char    spr_checkmap(unsigned int f_index);
extern unsigned char    spr_checkmap_down(unsigned int f_index);

extern void             spr_set_up();
extern void             spr_set_down();
extern void             spr_set_left();
extern void             spr_set_right();

extern unsigned char    spr_move_up(void);
extern unsigned char    spr_move_down(void);

extern unsigned char    spr_move_right(void);
extern unsigned char    spr_move_left(void);
extern void             spr_move_horizontal(void);

#ifdef GAME_JUMP
extern void             spr_new_jump(void);
extern void             spr_end_jump(void);
extern unsigned char    spr_move_jump(void);
extern unsigned char    spr_cango_jump(signed char f_inc);
extern unsigned char    spr_cango_fall(signed char f_inc);
#endif

extern void             spr_paint(void);

extern unsigned char    spr_set_tiledir(void);
extern void             spr_back_repaint(void);

extern void             spr_clear_scr(void);
extern void             spr_fill_lookup_table(void);

#endif
