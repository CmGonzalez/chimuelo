/*
   This file is part of PGD.

   PGD is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   PGD is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with PGD.  If not, see <http://www.gnu.org/licenses/>.
*/

//TODO RENAME THIS IS NOT AY IT'S UPPER MEM
#ifndef game_banks_H
#define game_banks_H

// AY mFX Effects Player

// Bit Flags Returned from ay_is_playing()
#define AY_PLAYING_NONE          0
#define AY_PLAYING_SONG          1
#define AY_PLAYING_FX            2
// AY Song Player
#define AY_SONG_LOOP             0
#define AY_SONG_ONCE             1

extern void ay_fx_play_mfx(unsigned char bank, void *effect) __preserves_regs(a,b,c) __z88dk_callee;
extern void ay_fx_stop_mfx(void) __preserves_regs(b,c,d,e,h,l);
extern void ay_song_play(unsigned char flag, unsigned char bank, void *song) __preserves_regs(b,c) __z88dk_callee;
extern void ay_song_stop(void) __preserves_regs(b,c,d,e,h,l);
extern void ay_reset_mfx(void) __preserves_regs(b,c,d,e,h,l);
extern unsigned char ay_is_playing_mfx(void) __preserves_regs(d,e);


// AY Voice + Midi Control
extern unsigned char     ay_is_playing(void) __preserves_regs(d,e);
extern void              ay_reset_voicenc(void) __preserves_regs(b,c);
extern unsigned char     ay_is_playing(void) __preserves_regs(b,c,d,e);
extern void              ay_midi_play(void *song) __preserves_regs(b,c) __z88dk_fastcall;
extern void              ay_fx_play_voicenc(void *effect) __preserves_regs(b,c) __z88dk_fastcall;



// AY Miscellaneous

// Voicenc Samples
extern unsigned char ay_effect_01[]; //SLIDE
extern unsigned char ay_effect_09[]; //SLIDE



//BANK 1 Images

//extern unsigned char cartoon3[];
//extern unsigned char logo1[];

//BANK 3 Sprites 16x16

//BANK 4 Songs
//BANK 4 Sound fx
extern unsigned char ay_fx_eat1[];
extern unsigned char ay_fx_eat2[];
extern unsigned char ay_fx_death[];
extern unsigned char ay_fx_ghost_eat[];
extern unsigned char ay_fx_fruit_eat[];
extern unsigned char ay_fx_fruit_target[];


//BANK 6 Songs
//BANK 7 Tiles 8x8


extern void dzx7Standard(unsigned char *src, unsigned char *dst);



#endif
