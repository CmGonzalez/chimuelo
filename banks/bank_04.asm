;; AY RELATED FOR BANK_4

; OWN COPY OF VTII PLAYER
PUBLIC vtii_init
PUBLIC vtii_play
PUBLIC vtii_stop
PUBLIC vtii_setup
PUBLIC vtii_modaddr

defc vtii_init  = INIT
defc vtii_play  = PLAY
defc vtii_stop  = STOP
defc vtii_setup = SETUP
defc vtii_modaddr = MODADDR

PUBLIC mfx_init
PUBLIC mfx_add
PUBLIC mfx_playm
PUBLIC mfx_playe
PUBLIC mfx_mfxptr

defc mfx_init  = MFXINIT
defc mfx_add   = MFXADD
defc mfx_playm = MFXPLAYM
defc mfx_playe = MFXPLAYE
defc mfx_mfxptr = mfxPtr



SECTION BANK_4_VTII

INCLUDE "src/VTII10bG-mfx.asm"

; OWN COPY OF MFXPLAYER

SECTION BANK_4_MFX

INCLUDE "src/mfx.asm"

; SONGS & EFFECTS IN THIS BANK

SECTION BANK_4_AUDIO
; MUSIC

;SOUND FX

PUBLIC _ay_fx_eat1
_ay_fx_eat1:
BINARY "assets/afx/eat1.afx"

PUBLIC _ay_fx_eat2
_ay_fx_eat2:
BINARY "assets/afx/eat2.afx"

PUBLIC _ay_fx_death
_ay_fx_death:
BINARY "assets/afx/death.afx"

PUBLIC _ay_fx_fruit_eat
_ay_fx_fruit_eat:
BINARY "assets/afx/fruit_eat.afx"

PUBLIC _ay_fx_ghost_eat
_ay_fx_ghost_eat:
BINARY "assets/afx/ghost_eat.afx"

PUBLIC _ay_fx_fruit_target
_ay_fx_fruit_target:
BINARY "assets/afx/fruit_target.afx"
