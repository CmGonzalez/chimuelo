;; Voicenc + MIDI - Based on WYZ Voicenc for MSX
;; ISR CALL

EXTERN _zx_enable_bank_00, _zx_enable_bank_06
EXTERN BANK06_ay_reset, _ay_reset_low
EXTERN BANK06_ay_fx_play_isr, _ay_fx_playing
EXTERN BANK06_ay_midi_play_isr, _ay_midi_playing, _ay_midi_hold
EXTERN _spec128, _GLOBAL_ZX_PORT_7FFD

; gather ay variables from main bank

; check for 128k model

   ; check for 128k model
   ld a,(_spec128)
   or a
   ret z

   ld hl,(_ay_fx_playing)
   push hl


   ld hl,(_ay_midi_playing)
   push hl

   ld a,(_ay_midi_hold)
   push af


   ld a,(_ay_reset_low)
   ld e,a


   ; enable bank 06

   call _zx_enable_bank_06



   ; restore bank 00

   call _zx_enable_bank_00

   ld (_ay_fx_playing),hl

   pop af
   ld (_ay_midi_hold),a

   pop hl
   ld (_ay_midi_playing),hl

   ld a,$ff
   ld (_ay_reset_low),a

   ;ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; BANKSWITCHING ;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SECTION code_crt_common

PUBLIC _zx_enable_bank_00, _zx_enable_bank_06

_zx_enable_bank_00:

   ld a,$10
   jr _zx_enable_bank_06 + 2

_zx_enable_bank_06:

   ld a,$16

   ld bc,$7ffd
   out (c),a

   ret
