;mFX ISR Service
EXTERN _ay_song_bank, _ay_song_command, _ay_song_command_param
EXTERN vtii_setup, vtii_stop, vtii_init, vtii_play

EXTERN _ay_fx_bank, _ay_fx_command, _ay_fx_command_param
EXTERN mfx_mfxptr, mfx_add, mfx_playm
EXTERN _spec128



; check for 128k model
ld a,(_spec128)
or a
ret z

; avoid contention problems
ld a,0x80
ld i,a

; process song command

song_command:

   ld a,(_ay_song_bank)
   or $10

   ld bc,$7ffd
   out (c),a                   ; page in song bank

   ld hl,vtii_setup
   set 1,(hl)                  ; enable stack output from ay song player

   ld a,(_ay_song_command)

   bit 1,a
   jr z, check_song_new

song_stop:

   call vtii_stop
   jr song_exit

check_song_new:

   bit 2,a
   jr z, song_play

song_new:

   and $01                     ; keep loop bit
   or $02                      ; enable stack output from ay song player
   ld (vtii_setup),a

   ld hl,(_ay_song_command_param)

   call vtii_init
   jr song_exit

song_play:

   call vtii_play

song_exit:

   xor a
   ld (_ay_song_command),a

   ; process fx command

fx_command:

   ld a,(_ay_fx_bank)
   or $10

   ld bc,$7ffd
   out (c),a                   ; page in fx bank

   ld a,(_ay_fx_command)

   bit 1,a
   jr z, check_fx_new

fx_stop:

   ld hl,0
   ld (mfx_mfxptr+1),hl

   jr fx_play

check_fx_new:

   bit 2,a
   jr z, fx_play

fx_new:

   ld hl,(_ay_fx_command_param)
   call mfx_add

fx_play:

   call mfx_playm

fx_exit:

   xor a
   ld (_ay_fx_command),a

   ; restore original bank

   ld a,(_GLOBAL_ZX_PORT_7FFD)

   ld bc,$7ffd
   out (c),a

   ; restore I

   ld a,0xfe
   ld i,a
   ;ret
