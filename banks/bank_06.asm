; MAP DATA & TILES

SECTION BANK_6_DATA

;; AY RELATED FOR


; MFX OWN COPY OF VTII PLAYER
SECTION BANK_6_VTII
INCLUDE "src/VTII10bG-mfx.asm"
; OWN COPY OF MFXPLAYER
SECTION BANK_6_MFX
INCLUDE "src/mfx.asm"


SECTION BANK_6_VOICENC

PUBLIC BANK06_ay_reset
PUBLIC BANK06_ay_midi_play_isr
PUBLIC BANK06_ay_fx_play_isr

defc BANK06_ay_reset  = _ay_reset_voicenc0
defc BANK06_ay_midi_play_isr  = _ay_midi_play
defc BANK06_ay_fx_play_isr  = _ay_fx_play_isr

INCLUDE "src/voicenc_midi.asm"
;INCLUDE "../src_map/dzx7_standard.asm"
SECTION BANK_06
;SECTION BANK_6_VTII


; SONGS & EFFECTS IN THIS BANK

SECTION BANK_6_AUDIO
defs 128 ;filler

SECTION BANK_6_MFX

PUBLIC _ay_effect_01
_ay_effect_01:
include "assets/sfx/ay_effect_01.asm"
defw -1


PUBLIC _ay_effect_09
_ay_effect_09:
include "assets/sfx/ay_effect_09.asm"
defw -1

; background effects only below this point

PUBLIC _ay_effect_background

defc _ay_effect_background = _ay_effect_19

PUBLIC _ay_effect_19

_ay_effect_19:

   include "assets/sfx/ay_effect_19.asm"
   defw $feff, _ay_effect_19
