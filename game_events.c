/*
        This file is part of PGD.

        PGD is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        PGD is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with PGD.  If not, see <http://www.gnu.org/licenses/>.


        PGD - Cristian Gonzalez - cmgonzalez@gmail.com
 */

#include "globals.h"
#include "game_events.h"
#include "src_engine/engine.h"
#include "src_engine/player.h"
#include "src_engine/sprite.h"
#include "src_engine/zx.h"
#include "src_engine/macros.h"
#include <arch/zx/nirvana+.h>
/*
player_move: Player (type 0) control	- player movement, reading keys, collision detection
Sprite type 1		- behaviour of sprites with type 1
Sprite type 2		- behaviour of sprites with type 2
Sprite type 3		- behaviour of sprites with type 3
Sprite type 4		- behaviour of sprites with type 4
Sprite type 5		- behaviour of sprites with type 5
Sprite type 6		- behaviour of sprites with type 6
Sprite type 7		- behaviour of sprites with type 7
Sprite type 8		- behaviour of sprites with type 8
Initialise sprite		- whenever a sprite is initialised
Main loop 1			- every game loop
Main loop 2			- every game loop
Intro/menu			- before the game begins
game_init: Game initialisation	- at start of game, set up variables
Restart screen		- what happens when player restarts a screen
Fell too far		- when any sprite reaches the end of the jump table
Kill player			- happens when player loses a life with a KILL command
Lost game			- when the player loses his last life
Completed game		- when game completed successfully, eg
*/

unsigned char game_init() {

  player_start_lin = 160;
  player_start_col = 20;
  player_start_tile = 16; //TILE_P1_RIGHT
  scr_curr = 7;
  game_border = INK_BLACK;
  return 0;
}

void game_event() {



}

unsigned char player_move(void) {

  //PLATFORMS
  /* Player on Air
  */
  sprite->spr_speed = 4;
  if (sprite->state & BIT_JUMP) {
    sprite->spr_speed = 3;

    sprite->base_tile = player_start_tile + 16;
    spr_set_tiledir();
    spr_move_jump();
    return 0;
  }

  if ( sprite->state & BIT_DOWN ) {
    sprite->spr_speed = 2;

    spr_move_down();
    return 0;
  }
  /* Read Player Input */
  dirs = player_read_input();


if ((dirs & IN_STICK_FIRE)  && !(sprite->state & BIT_ATTACK)) {
    sprite->base_tile = player_start_tile + 16;
    spr_set_tiledir();
    if (sprite->state_l & BIT_RIGHT) {
      gbyte = 20 + 1 + &btiles[0] + (spr_set_tiledir() * 48);
      *gbyte = 240;
      *(gbyte+2) = 224;
      gbyte+=48;
      *gbyte = 60;
      *(gbyte+2) = 56;
      gbyte+=48;
      *gbyte = 15;
      *(gbyte+2) = 14;
    } else {
      gbyte = 20 + 2 + &btiles[0] + (spr_set_tiledir() * 48);
      *gbyte = 240;
      *(gbyte+2) = 224;
      gbyte+=48;
      *gbyte = 60;
      *(gbyte+2) = 56;
      gbyte+=48;
      *gbyte = 15;
      *(gbyte+2) = 14;
    }
    sprite->state |= BIT_ATTACK;
    sprite->last_time_a  = zx_clock();
}



  if (dirs & IN_STICK_UP) {
    spr_new_jump();
  }

  if (dirs & IN_STICK_LEFT) {
    sprite->base_tile = player_start_tile;
    spr_set_left();
    if (spr_cango_left()) spr_move_left();
    return 0;
  }
  if (dirs & IN_STICK_RIGHT) {
    sprite->base_tile = player_start_tile;
    spr_set_right();
    if (spr_cango_right()) spr_move_right();
    return 0;
  }
  //No input from the user
  if (sprite->state &&  ( sprite->state & ~BIT_ATTACK ) ) {
    sprite->state_l = sprite->state;
    sprite->state &= ~BIT_LEFT;
    sprite->state &= ~BIT_RIGHT;
    sprite->base_tile = player_start_tile + 8;
    spr_set_tiledir();
  }

  if (sprite->state & BIT_ATTACK) {
    if (zx_clock() - sprite->last_time_a > 16) {
      sprite->state &= ~BIT_ATTACK;
      sprite->base_tile = player_start_tile;
      spr_set_tiledir();
      if (sprite->state_l & BIT_RIGHT) {
        gbyte = 20 + 1 + &btiles[0] + (spr_set_tiledir() * 48);
        *gbyte = 0;
        *(gbyte+2) = 0;
        gbyte+=0;
        *gbyte = 0;
        *(gbyte+2) = 0;
        gbyte+=0;
        *gbyte = 0;
        *(gbyte+2) = 0;
      } else {
        gbyte = 20 + 2 + &btiles[0] + (spr_set_tiledir() * 48);
        *gbyte = 0;
        *(gbyte+2) = 0;
        gbyte+=48;
        *gbyte = 0;
        *(gbyte+2) = 0;
        gbyte+=48;
        *gbyte = 0;
        *(gbyte+2) = 0;
      }
    }
  }
  return 0;
}

void player_pick_spr() {
  /*
  unsigned int f_score;
  NIRVANAP_spriteT(NIRV_SPRITE_FG, 0, 0, 0);
  NIRVANAP_fillT(INK_BLACK | PAPER_BLACK, lin[INDEX_FRUIT], col[INDEX_FRUIT]);
  audio_eat_fruit();
  game_scr_val();
  f_score = scr_score[v0];
  player_score_add(f_score);
  spr_kind[INDEX_FRUIT] = E_NONE;
  last_time[INDEX_FRUIT] = zx_clock();
  --spr_count;
  */
}
