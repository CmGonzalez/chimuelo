# chimuelo

Proyect Chimuelo - Ultra Secret

How to Building

For OS X builds please install Command Line Tools   
sudo xcode-select -s /Library/Developer/CommandLineTools

For Linux (Ubuntu) please install Build Essentials   
sudo apt-get install build-essentials

1.- Install z88dk 1.99c
wget https://github.com/z88dk/z88dk/releases/download/v1.99c/z88dk-src-1.99C.tgz
cd z88dk
chmod 777 build.sh
./build.sh

2.- Configure z88dk path
Edit game_config.mk change the Z88DK_PATH  variable to the installation dir of z88dk

3.- Make Build tools
run make build-toos to compile the build tools needed by the framework.

4.- Configure NIRVANA+
run make nirvana-config to edit z88dk Nirvana+ file

Disable Wide Sprites (Not used currently)
define(`__NIRVANAP_OPTIONS_WIDE_DRAW',    0x00)
define(`__NIRVANAP_OPTIONS_WIDE_SPRITES', 0x00)

Define the numbers or Rows to be used by Nirvana+
define(`__NIRVANAP_TOTAL_ROWS', 22)      # total number of rows drawn by nirvana+ 1-23

5.- Compile NIRVANA+

Select one of the 3 versions of nirvana to be used on the project (nirvana-std, nirvana-spr8 or nirvana-spr8) (currently nirvana-std is not working).

Run make nirvana-spr8 ot nirvana-spr8t

6.- Build your Map
Run make map to compile your Tiled game_map.json  on assets/map/ this will create a file game_map.h with a proper C array with all the data compressed by zx7.

7.- Clean your build tree (optional)
Run make clean to clean your build tree.

8.- Compile your Game

Run make or make -j10 to compile



Good Lucks!
